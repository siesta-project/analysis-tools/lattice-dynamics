C...............................................................
C
      subroutine stre_peaks(io1,nat,ityp,label,mass,tau,coor,
     .                      ityp1,ityp2,bmin,bmax,
     .	                    ivmin,ivmax,freq,evr,evi,wwsum)
C
C     Sums squares of bond stretchings between all atoms
C     of types 'ityp1' and 'ityp2', with bond lengths 
C     between 'bmin' and 'bmax'.
C
      implicit none
      integer io1,iev,nat,ityp(nat),
     .        iat,jat,nrat,ii,jj,ityp1,ityp2,ivmin,ivmax,ix,iy,iz
      double precision freq(ivmin:ivmax),
     .                 evr(3,nat,ivmin:ivmax),evi(3,nat,ivmin:ivmax),
     .                 wwsum(ivmin:ivmax),
     .                 tau(3,3),coor(3,nat),mass(nat),
     .                 bmin,bmax,bmin2,bmax2,dist2
      character*2 label(nat)
      logical match1,match2

      write (io1,201) ityp1,ityp2,bmin,bmax
C
      bmin2 = bmin*bmin
      bmax2 = bmax*bmax
      wwsum = 0.d0
      do iat = 1,nat
      do jat = iat,nat
        match1 = (ityp(iat).eq.ityp1 .and. ityp(jat).eq.ityp2)
        match2 = (ityp(iat).eq.ityp2 .and. ityp(jat).eq.ityp1)
        if (match1 .or. match2) then
C ---     this pair is OK; check distances... Allow translations +/- 1
          do ix=-1,1
          do iy=-1,1
          do iz=-1,1
            dist2= (coor(1,jat) - coor(1,iat) +
     +      tau(1,1)*ix + tau(1,2)*iy + tau(1,3)*iz )**2 +
     +             (coor(2,jat) - coor(2,iat) +
     +      tau(2,1)*ix + tau(2,2)*iy + tau(2,3)*iz )**2 +
     +             (coor(3,jat) - coor(3,iat) +
     +      tau(3,1)*ix + tau(3,2)*iy + tau(3,3)*iz )**2 
            if (dist2.gt.bmin2 .and. dist2.lt.bmax2) then
C ---         a "good" bond found; add it to the list... 
              write (io1,202) iat,coor(1:3,iat),jat,coor(1:3,jat),
     .                        ix,iy,iz,sqrt(dist2)
C ---         ... and update entris for all modes in wwsum().
C             Divide by sqrt(mass) to extract bare displacements :
              do iev=ivmin,ivmax
                do ii=1,3
                  wwsum(iev) = wwsum(iev) +
     +            ( evr(ii,jat,iev)/sqrt(mass(jat)) -
     -              evr(ii,iat,iev)/sqrt(mass(iat)) )**2 + 
     +            ( evi(ii,jat,iev)/sqrt(mass(jat)) -
     -              evi(ii,iat,iev)/sqrt(mass(iat)) )**2
                enddo
              enddo
            endif
          enddo
          enddo
          enddo   !   -- do ix...
        endif   
      enddo
      enddo   !   -- do iat...

      write (io1,203) 
      do iev=ivmin,ivmax
        write (io1,204) freq(iev),wwsum(iev)
      enddo
      return

  201 format('#  Sum of squares of bond stretchings between atoms',
     .       ' of types',i4,' and',i4,/
     .       '#  at distances from',f9.5,' to',f9.5,' Ang.'/
     .       '#   Paires added:'/
     .       '#  iat  ',10('-'),' coord ',9('-'),'   jat   ',10('-'),
     .       ' coord ',9('-'),' translated    dist.'/'#')
  202 format('#',i4,3f10.4,i5,3f10.4,3i3,f12.5)
  203 format('#',/'#   Freq  Displ. squared',/
     .       '#  (cm-1) ',/'#') 
  204 format (f10.3,f10.4)
      end
C
C...............................................................
C
      subroutine stre_smear(delta,npoints,io2,ivmin,ivmax,nat,
     .                      ityp1,ityp2,bmin,bmax,label,freq,wwsum)
C     Smear the peaks of phonon density of states 
C     (frequencies in  "freq", squared eigenvectors in "wwsum")
C     with a width parameter delta
C
      implicit none
      integer npoints,io2,iev,ivmin,ivmax,ii,nat,ityp1,ityp2
      double precision freq(ivmin:ivmax),wwsum(ivmin:ivmax),
     .       bmin,bmax,zz,xx,delta,xmin,xmax,xstep,broad,qq(3)
      character*2 label(nat)
      external broad

C     determine limits and mesh, to avoid many questions:
      xmin = freq(ivmin) - (freq(ivmax)-freq(ivmin))*0.15
      xmax = freq(ivmax) + (freq(ivmax)-freq(ivmin))*0.15
      xstep=(xmax-xmin)/(npoints-1)

      write (io2,201) ityp1,label(ityp1),ityp2,label(ityp2),
     .                bmin,bmax,delta
      write (io2,202) 

      do ii = 1,npoints
        xx = xmin+(ii-1)*xstep
        zz = 0.d0
        do iev=ivmin,ivmax
          zz = zz + broad(xx-freq(iev),delta)*wwsum(iev)
        enddo
        write (io2,203) xx,zz
      enddo
      return

  201 format('#  Sum of squares of bond stretchings between atoms',
     .       ' of type',i4,' (',a2,')   and of type',i4,'(',a2,')'/
     .       '#  at distances from',f9.5,' to',f9.5,
     .       ' Ang,   smeared with ',f8.4)
  202 format('#',/'#    Freq   Displ. squared',/
     .       '#  (cm-1) ',/'#') 
  203 format (f10.3,f12.6)
      end
C
C...............................................................
C
      function broad(xx,dd)
      implicit none
      double precision broad,xx,dd
C     Explicitly defined broadening function:
C     e.g. Lorentz:
      broad=dd/3.1415926536/(xx*xx+dd*dd)
      return
      end
