C...............................................................
C
      subroutine param1n(nz1,nz2,dist,fcdiag)
C
C     contains fit (model) data of force constants between NN
C     for different element pairs.
C     Presumably the dependence is on the distance only,
C     and the orientation is set in subr. block1n.
C
C     Update the parametrization below for each new pair of elements.
C
C     Input:
C       nz1          : Z-value for 1st atom
C       nz1          : Z-value for 2d  atom
C       dist         : distance between the two atoms in Ang
C     Output:
C       fcdiag       : diagonal elements (in decreasing order)
C                      of force constant matrix
C                      Units: eV/Ang**2
C
      implicit none
      integer nz1,nz2
      double precision dist,fcdiag(3)

      if ((nz1.eq.4.and.nz2.eq.34).or.
     .    (nz1.eq.34.and.nz2.eq.4)) then
C ----- Be-Se -----
        fcdiag(1) = 20. - 0.5 * dist
        fcdiag(2) = 0.5 
        fcdiag(3) = 0.5 
      elseif ((nz1.eq.30.and.nz2.eq.34).or.
     .        (nz1.eq.34.and.nz2.eq.30)) then
C ----- Zn-Se -----
        fcdiag(1) = 20. - 0.5 * dist
        fcdiag(2) = 0.5 
        fcdiag(3) = 0.5 
      elseif (nz1.eq.14.and.nz2.eq.14) then
C ----- Si-Si -----
        fcdiag(1) = 108.27 - 42.67 * dist
        fcdiag(2) = 0.95
        fcdiag(3) = 0.95
      elseif ((nz1.eq.14.and.nz2.eq.5).or.
     .        (nz1.eq.5.and.nz2.eq.14)) then
C ----- Si-B -----
        fcdiag(1) = 7.36
        fcdiag(2) = 1.02 
        fcdiag(3) = 1.02
      elseif ((nz1.eq.31.and.nz2.eq.33).or.
     .        (nz1.eq.33.and.nz2.eq.31)) then
C ----- Ga-As -----
        fcdiag(1) = 83.636 - 31.285 * dist
        fcdiag(2) = -3.613 +  1.657 * dist
        fcdiag(3) = -3.613 +  1.657 * dist
      elseif ((nz1.eq.49.and.nz2.eq.33).or.
     .        (nz1.eq.33.and.nz2.eq.49)) then
C ----- In-As -----
        fcdiag(1) = 86.997 - 30.792 * dist
        fcdiag(2) = -4.533 +  1.848 * dist
        fcdiag(3) = -4.533 +  1.848 * dist

      else
        write (6,201) nz1,nz2
        stop
      endif

      return
  201 format('  Stop in param1n: no fit data specified for nz1=',
     .        i3,',   nz2=',i3,' : update subr. param1n !')
      end
C
C...............................................................
C
      subroutine param2n(nz1,nz2,nz3,dist,thet1,thet2,fcdiag)
C
C     contains fit (model) data of force constants between NNN
C     for different element pairs.
C
C     Update the parametrization below for each new pair of elements.
C
C     Input:
C       nz1          : Z-value for 1st atom
C       nz2          : Z-value for 2d  atom
C       nz3          : Z-value for 3d (intermediate) atom
C       dist         : distance between the two atoms
C     Output:
C       thet1, 
C       thet2        : deviation of main eigenvector  
C                      from atom-to-atom direction
C                      (towards the intermediate atom), in degrees 
C       fcdiag       : diagonal elements (in decreasing order)
C                      of force constant matrix
C                      Units: eV/Ang**2
C
      implicit none
      integer nz1,nz2,nz3
      double precision dist,thet1,thet2,fcdiag(3),rad2grd
      data rad2grd/57.29577951289617186797/    !  radian

      if ((nz1.eq.14).and.(nz2.eq.14).and.(nz3.eq.14)) then
C ----- Si-(Si)-Si -----
        fcdiag(1) = 4.191 - 0.935 * dist
        fcdiag(2) = 1.330 - 0.283 * dist
        fcdiag(3) = -0.1413 + 0.0589 * dist
        thet1 = 16.00
        thet2 = 16.00
      elseif ((nz1.eq.5).and.(nz2.eq.14).and.(nz3.eq.14)) then
C ----- B-(Si)-Si -----
        fcdiag(1) = 0.780
        fcdiag(2) = 0.344
        fcdiag(3) = 0.086
        thet1     = 14.00
        thet2     = 19.00
      elseif ((nz1.eq.14).and.(nz2.eq.5).and.(nz3.eq.14)) then
C ----- Si-(Si)-B -----
        fcdiag(1) = 0.780
        fcdiag(2) = 0.344
        fcdiag(3) = 0.086
        thet1     = 19.00
        thet2     = 14.00
      elseif ((nz1.eq.14).and.(nz2.eq.14).and.(nz3.eq.5)) then
C ----- Si-(B)-Si -----
        fcdiag(1) = 4.191 - 0.935 * dist
        fcdiag(2) = 1.330 - 0.283 * dist
        fcdiag(3) = -0.1413 + 0.0589 * dist
        thet1 = 9.20
        thet2 = 9.20

      elseif ((nz1.eq.33).and.(nz2.eq.33).and.(nz3.eq.31)) then
C ----- As-(Ga)-As -----
        fcdiag(1) = 4.1366 - 0.8652 * dist
        fcdiag(2) = 0.040
        fcdiag(3) = 0.020
        thet1 = rad2grd * acos(1.4181 - 0.1261 * dist)
C       thet1 = rad2grd * acos(0.91)
        thet2 = thet1
      elseif ((nz1.eq.33).and.(nz2.eq.33).and.(nz3.eq.49)) then
C ----- As-(In)-As -----
        fcdiag(1) = 2.2781 - 0.4305 * dist
        fcdiag(2) = 0.040
        fcdiag(3) = 0.020
        thet1 = rad2grd * acos(1.4561 - 0.1272 * dist)
C       thet1 = rad2grd * acos(0.91)
        thet2 = thet1
      elseif ((nz1.eq.31).and.(nz2.eq.31).and.(nz3.eq.33)) then
C ----- Ga-(As)-Ga -----
        fcdiag(1) =  1.4316 - 0.1845 * dist
        fcdiag(2) =  4.4273 - 1.0144 * dist
        fcdiag(3) = -0.5956 + 0.1552 * dist
        thet1 = rad2grd * acos(6.2656 - 1.3818 * dist)
        thet2 = thet1
      elseif ((nz1.eq.49).and.(nz2.eq.49).and.(nz3.eq.33)) then
C ----- In-(As)-In -----
        fcdiag(1) = 0.3863 + 0.0578 * dist
        fcdiag(2) = 1.3119 - 0.2483 * dist
        fcdiag(3) = 0.010
        thet1 = rad2grd * acos(2.6853 - 0.4535 * dist)
        thet2 = thet1
      elseif ((nz1.eq.31).and.(nz2.eq.49).and.(nz3.eq.33)) then
C ----- Ga-(As)-In -----
        fcdiag(1) = -0.7788 + 0.3443 * dist
        fcdiag(2) =  1.1112 - 0.1991 * dist
        fcdiag(3) = -0.3961 + 0.0996 * dist
        thet1 = rad2grd * acos(1.0086 - 0.0739 * dist)
        thet2 = rad2grd * acos(0.8828 - 0.0419 * dist)
      elseif ((nz1.eq.49).and.(nz2.eq.31).and.(nz3.eq.33)) then
C ----- In-(As)-Ga -----
        fcdiag(1) = -0.7788 + 0.3443 * dist
        fcdiag(2) =  1.1112 - 0.1991 * dist
        fcdiag(3) = -0.3961 + 0.0996 * dist
        thet1 = rad2grd * acos(0.8828 - 0.0419 * dist)
        thet2 = rad2grd * acos(1.0086 - 0.0739 * dist)
      
      else
        write (6,201) nz1,nz2,nz3
        stop
      endif

      return
  201 format('  Stop in param2n: no fit data specified for nz1=',i3,
     .       ', nz2=',i3,', nz3=',i3,' : update subr. param2n !')
      end
