      program zerofc
C
C     zero out elements of .FC (from Siesta) between atoms which are
C     more distant than a certain condition (isotropically or anisotropically)
C     written by Andrei Postnikov
C     postnikov@univ-metz.fr
C
      implicit none
      character ii1nam*60, ii2nam*60, io1nam*60
C     character l1*1(3), l2*1(2), amode*1
      character*1 l1(3), l2(2), amode
      data l1,l2 /'x','y','z','+','-'/
      integer nat,ii1,ii2,io1,ii,jj,iat,jat,ialloc,mdisp,
     .        idisp,jdisp,kdisp,ix,jx,ipm,iline,mode
      integer, allocatable :: ntyp(:), nz(:), neigh(:,:)
      logical keepit
      double precision cc_bohr(3,3), zero,rfmax,dxmax,dymax,dzmax,
     .                 xdisp,ydisp,zdisp,dx,dy,dz,dist
      double precision, allocatable :: fc_in(:,:,:,:,:),
     .                  coor_bohr(:,:), fc_out(:,:,:,:,:)
      parameter (ii1=11,ii2=12,io1=13,zero=0.d0,mdisp=2)
C
C --- open and read .XV:
      write (6,201,advance="no")
      read (5,*) ii1nam
      open (ii1,file=ii1nam,form='formatted',status='old',err=801)
      write (6,*) 'Found and opened: ',ii1nam
      do ii=1,3
        read  (ii1,101)  (cc_bohr(jj,ii),jj=1,3) 
      enddo
C     read (ii1,'(i12)') nat
      read (ii1,*) nat
      allocate (ntyp(nat),neigh(nat,nat),nz(nat),
     .          coor_bohr(3,nat),fc_in(3,nat,3,nat,2),
     .          fc_out(3,nat,3,nat,2),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      do iat=1,nat
        read (ii1,103,end=801,err=802) ntyp(iat),nz(iat),
     .                           (coor_bohr(ii,iat),ii=1,3)
      enddo
      close (ii1)
C --- open and read .FC:
      write (6,202,advance="no")
      read (5,*) ii2nam
      open (ii2,file=ii2nam,form='formatted',status='old',err=801)
      write (6,*) 'Found and opened: ',ii2nam
      read (ii2,*)
      iline = 0
      do iat=1,nat   !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat   !   atom at which force is induced
          iline = iline + 1
          read (ii2,104,err=301,end=302) 
     .         (fc_in(jx,jat,ix,iat,ipm),jx=1,3)
        enddo
      enddo
      enddo
      enddo
      close (ii2)
C --- do something...
  400 write (6,*) ' You want to zero out force constants',
     .            ' Isotropically, beyond a given radius - then type I'
      write (6,*) ' or Anisotropically, beyond a given Cartesian',
     .            ' vector - then type A :'
      read  (5,*)  amode
      if (amode.eq.'I'.or.amode.eq.'i') then
        mode = 1
  401   write (6,*) ' Zero out force constants beyond distance',
     .              ' (in Bohr):'
        read (5,*,err=401) rfmax
      elseif (amode.eq.'A'.or.amode.eq.'a') then
        mode = 2
  402   write (6,*) ' Zero out force constants beyond +/- three',
     .              ' components of a Cartesian vector (in Bohr):'
        read (5,*,err=402) dxmax,dymax,dzmax
      else
        write (6,*) ' Not clear, please enter I or A'
        goto 400
      endif
     
      loop_iat:  do iat=1,nat
        loop_jat: do jat=1,nat
C         zero out all fc_out and then update for close atoms:
          fc_out(:,jat,:,iat,:) = 0.d0
          loop_disp: do idisp=-mdisp,mdisp
          do jdisp=-mdisp,mdisp
          do kdisp=-mdisp,mdisp
            xdisp = cc_bohr(1,1)*idisp + 
     .              cc_bohr(1,2)*jdisp + 
     .              cc_bohr(1,3)*kdisp
            ydisp = cc_bohr(2,1)*idisp + 
     .              cc_bohr(2,2)*jdisp + 
     .              cc_bohr(2,3)*kdisp
            zdisp = cc_bohr(3,1)*idisp + 
     .              cc_bohr(3,2)*jdisp + 
     .              cc_bohr(3,3)*kdisp
            dx = coor_bohr(1,iat)-coor_bohr(1,jat) + xdisp
            dy = coor_bohr(2,iat)-coor_bohr(2,jat) + ydisp
            dz = coor_bohr(3,iat)-coor_bohr(3,jat) + zdisp
            if (mode.eq.1) then
              dist = sqrt(dx**2 + dy**2 + dz**2)
              if (dist.lt.rfmax) then
                keepit = .true.  
              else
                keepit = .false.
              endif
            elseif (mode.eq.2) then
              if ( (abs(dx).lt.abs(dxmax)).and.
     .             (abs(dy).lt.abs(dymax)).and.
     .             (abs(dz).lt.abs(dzmax)) ) then
                keepit = .true.
              else
                keepit = .false.
              endif
            else 
              write (6,*) ' Oops: mode=',mode,' is illegal!'
              stop
            endif
            if (keepit) then
C ---         copy FC to output file
              fc_out(:,jat,:,iat,:) = fc_in(:,jat,:,iat,:)
              exit loop_disp
            endif
          enddo
          enddo
          enddo loop_disp
        enddo loop_jat
      enddo loop_iat
C --- write modified  .FC:
      write (6,203,advance="no")
      read (5,*) io1nam
      open (io1,file=io1nam,form='formatted',status='new',err=803)
      if (mode.eq.1) then
        write (io1,204) rfmax
      elseif (mode.eq.2) then
        write (io1,205) dxmax,dymax,dzmax
      endif
      do iat=1,nat   !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat   !   atom at which force is induced
C         write (io1,104) (fc_out(jx,jat,ix,iat,ipm),jx=1,3) 
          write (io1,105) (fc_out(jx,jat,ix,iat,ipm),jx=1,3), 
     .                    iat,l2(ipm),l1(ix),jat
        enddo
      enddo
      enddo
      enddo
      close (io1)
      deallocate (ntyp,neigh,nz,coor_bohr,fc_in,fc_out)
      stop

  101 format(3x,3f18.9)
  103 format(i3,i6,3f18.9)
  104 format (3f15.7)
  105 format (3f15.7,3x,'disp.',i5,2a1,', force at',i5)
  201 format (' Full name of .XV file: ')
  202 format (' Full name of .FC file: ')
  203 format (' Name of modified .FC file: ')
  204 format ('Force constants matrix suppressed beyond ',f12.6,' Bohr')
  205 format ('Force constants matrix suppressed beyond ',
     .        '(', 3f10.6,' ) Bohr')
  801 write (6,*) ' Error opening file as old formatted'
      stop
  802 write (6,*) ' End/Error reading XV for atom number ',iat
      stop
  803 write (6,*) ' Error opening file as new formatted'
      stop
  301 continue
      print *,' Error reading FC file, line ',iline
      stop
  302 continue
      print *,' Unexpected end of FC file, iat,ix,ipm,jat=',
     .         iat,ix,ipm,jat     
      stop

      end
      
