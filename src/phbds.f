C
C   phbnd, a script to calculte relative intensities 
C          of bond stretching / bending,
C          using phonon eigenvectors provided by "vibrator"

C          Written by Andrei Postnikov 
C            Split-off from phdos, Nov 2014
C          andrei.postnikov@univ-lorraine.fr
C
      program phbnd
      implicit none
      integer ii1,ii2,io1,io2,ivmin,ivmax
      parameter (ii1=11,ii2=12,io1=14,io2=15)
      integer ialloc,iat,nat,iatmin,iatmax,mode,npoints
      integer ityp1,ityp2,ityp3
      logical fund1,fund2,fund3
      integer, allocatable :: ityp(:),iz(:),jat(:,:),jtyp(:),nna(:)
      double precision tau(3,3),qq(3),delta,bmin,bmax
      double precision, allocatable :: mass(:),coor(:,:),
     .       freq(:),evr(:,:,:),evi(:,:,:),wwsum(:)
      character inpfil*60,outfil*60,syslab*30,qlab*1
      character*2, allocatable :: label(:)
      external test_xv,read_xv,read_ev,stre_peaks,stre_smear
C
C     string manipulation functions in Fortran used below:
C     len_trim(string): returns the length of string
C                       without trailing blank characters,

C --- read lattice vectors and atom positions from the .XV file:
      write (6,701)
  701 format(' Specify SystemLabel of .XV file: ',$)
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.XV'
      open (ii1,file=inpfil,form='formatted',status='old',err=801)
      call test_xv(ii1,nat)
      allocate (ityp(nat))
      allocate (iz(nat))
      allocate (mass(nat))
      allocate (label(nat))
      allocate (jtyp(nat))
      allocate (nna(nat))
      allocate (jat(nat,nat))
      allocate (coor(1:3,1:nat),STAT=ialloc) 
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      call read_xv(ii1,nat,ityp,iz,tau,mass,label,coor)
      close (ii1)                                                     
C --- read and store frequencies /eigenvectors from the .vector file,
C     q =(0 0 0)  only :
C     allocate for all modes, 1 through nat*3 :
      ivmin=1
      ivmax=nat*3
C     (change ivmin, ivmax above if want to restrict to less modes)
      allocate (freq(ivmin:ivmax))
      allocate (evr(1:3,1:nat,ivmin:ivmax))
      allocate (evi(1:3,1:nat,ivmin:ivmax))
      allocate (wwsum(ivmin:ivmax),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for vibration modes'
        stop
      endif
      write (6,702)
  702 format(' Specify SystemLabel of .vectors file: ',$)
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.vectors'
      open (ii2,file=inpfil,form='formatted',status='old',err=801)
      call read_ev(ii2,nat,qq,ivmin,ivmax,evr,evi,freq)
      write (6,*) 'opened and read ',inpfil
      close (ii2)
C ---------------------------------------------------
      write (6,*) 'You have two options: Calculate effect of ',
     .            'stretching of bonds between two atom species A1-A2,'
      write (6,*) 'or effect of bending bonds A1-A2-A3. '
  101 write (6,703) 
  703 format(' Do you want the efect of stretching (S) ',
     .       ' or bending (B) ? : ',$)
      read (5,*,err=101,end=101) qlab
      if (qlab.eq.'S'.or.qlab.eq.'s') then
        mode=1
  102   write (6,*) 'Type two atom types specifying bond stretching:'
        read (5,*,err=102,end=102) ityp1,ityp2
C       check if such atom types exist:
        fund1=.false.
        fund2=.false.
        do iat=1,nat
	  if (ityp(iat).eq.ityp1) fund1=.true.
	  if (ityp(iat).eq.ityp2) fund2=.true. 
	enddo
	  if (.not.fund1) write (6,704) ityp1
	  if (.not.fund2) write (6,704) ityp2
	  if ((.not.fund1).or.(.not.fund2)) goto 102
  704   format (' No such atom type in XV file :',i5)
      else if (qlab.eq.'B'.or.qlab.eq.'b') then
        mode=2
  103   write (6,*) 'Type three atom types specifying bond bending:'
        read (5,*,err=103,end=103) ityp1,ityp2,ityp3
C       check if such atom types exist:
        fund1=.false.
        fund2=.false.
        fund3=.false.
        do iat=1,nat
	  if (ityp(iat).eq.ityp1) fund1=.true.
	  if (ityp(iat).eq.ityp2) fund2=.true. 
	  if (ityp(iat).eq.ityp3) fund3=.true. 
	enddo
	  if (.not.fund1) write (6,704) ityp1
	  if (.not.fund2) write (6,704) ityp2
	  if (.not.fund3) write (6,704) ityp3
	  if ((.not.fund1).or.(.not.fund2).or.(.not.fund3)) goto 103
      else
        goto 101
      endif
  104 write (6,*) 'Define min. and max. bond length (in Ang) :'
      read (5,*,err=104,end=104) bmin,bmax
      if (bmin.ge.bmax) goto 104

C ---------------------------------------------------
C --- output file(s) .WS? contain the same information as .VS?,
C     but smeared, as an (continuous) functions of frequency. 
C     Fix smearing parameter and No. of steps, to avoid many questions:
C     delta = 10.0
      delta =  2.0
      npoints = 2000
C 
      if (mode.eq.1) then
        outfil = syslab(1:len_trim(syslab))//'.VSS'
        open (io1,file=outfil,form='formatted',status='new',err=802)
        call stre_peaks(io1,nat,ityp,label,mass,tau,coor,
     .                  ityp1,ityp2,bmin,bmax,
     .                  ivmin,ivmax,freq,evr,evi,wwsum)       
        close (io1)
        outfil = syslab(1:len_trim(syslab))//'.WSS'
        open (io2,file=outfil,form='formatted',status='new',err=802)
        call stre_smear(delta,npoints,io2,ivmin,ivmax,nat,
     .                  ityp1,ityp2,bmin,bmax,label,freq,wwsum)
        close (io2)
      else if (mode.eq.2) then
        outfil = syslab(1:len_trim(syslab))//'.VSB'
        open (io1,file=outfil,form='formatted',status='new',err=802)
!       call bend_peaks(io1,nat,jat,label,jtyp,nna,coor,
!    .                  ivmin,ivmax,freq,evr,evi,wwsum)       
        close (io1)
        outfil = syslab(1:len_trim(syslab))//'.WSB'
        open (io2,file=outfil,form='formatted',status='new',err=802)
!       call bend_smear(delta,npoints,io2,ivmin,ivmax,nat,jat,qq,
!    .                  nodif,jtyp,label,freq,wwsum)
        close (io2)
      endif
C
      deallocate (ityp,iz,mass,label,jtyp,nna,jat,coor)
      deallocate (freq,evr,wwsum)
      stop
                                                           
  801 write (6,*) ' Error opening file ',
     .            inpfil(1:len_trim(inpfil)),' as old formatted'
      stop 
  802 write (6,*) ' Error opening file ',
     .            outfil(1:len_trim(inpfil)),' as new'
      stop 
      end
C
C...............................................................
