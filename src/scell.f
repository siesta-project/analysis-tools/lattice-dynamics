      program scell
C
C     a script co construct supercells representing a mixed system,
C     subject to desired constraints.
C
C     Input: prototype single cell;
C            supercell vectors;
C            symmetry operations;
C            number of A/B atoms to be distributed over the sites.
C     Output: a list of non-trivial 
C             (not related by translations and rotations) 
C             supercells.
C
      implicit none
      character ii1nam*60,io1nam*60
      integer nat,ii1,io1,ii,jj,iat,ntyp,ityp,nsym,isym,ialloc,
     .        mat(3,3,48),mtau(3,3),nmult
      integer, allocatable :: noat(:),norig(:)
      parameter (ii1=1,io1=12)
      double precision tau(3,3),wsum,small,s_tau(3,3)
      double precision, allocatable :: coor0(:,:),coor1(:,:),
     .                  coor2(:,:),ww(:,:)
      data small/0.0001/
      external expand

      write (6,"(' Input  file : ')",advance="no")
      read (5,*) ii1nam
      open (ii1,file=ii1nam,form='formatted',status='old',err=801)
C --- read vectors of the prototype (single) cell:
      read (ii1,*,err=802) (tau(ii,1),ii=1,3)
      read (ii1,*,err=802) (tau(ii,2),ii=1,3)
      read (ii1,*,err=802) (tau(ii,3),ii=1,3)
C --- read supercell multiplication vectors:
      read (ii1,*,err=803) (mtau(ii,1),ii=1,3)
      read (ii1,*,err=803) (mtau(ii,2),ii=1,3)
      read (ii1,*,err=803) (mtau(ii,3),ii=1,3)
      nmult = mtau(1,1)*mtau(2,2)*mtau(3,3) +
     +        mtau(1,2)*mtau(2,3)*mtau(3,1) +
     +        mtau(1,3)*mtau(2,1)*mtau(3,2) -
     -        mtau(1,1)*mtau(2,3)*mtau(3,2) -
     -        mtau(1,2)*mtau(2,1)*mtau(3,3) -
     -        mtau(1,3)*mtau(2,2)*mtau(3,1)  
      nmult = abs(nmult)
C --- 
      write (6,"(' Output file : ')",advance="no")
      read (5,*) io1nam
      open (io1,file=io1nam,form='formatted',status='unknown')
C --- write down supercell lattice vectors into the output file
C     in the XCrySDen format:
      write (io1,"('CRYSTAL',/'PRIMVEC')")
      do ii=1,3
        s_tau(:,ii) = tau(:,1)*mtau(1,ii) + 
     +                tau(:,2)*mtau(2,ii) +
     +                tau(:,3)*mtau(3,ii)
        write(io1,"(3f13.6)") s_tau(:,ii)
      enddo
      write (io1,"('PRIMCOORD')")
C --- read number of atoms and species in the prototype cell:
      read (ii1,*) nat,ntyp
      allocate (coor0(3,nat),
     .          coor1(3,nat*nmult),coor2(3,nat*nmult),
     .          ww(ntyp,nat),noat(ntyp),
     .          norig(nat*nmult),
     .          STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
C --- read atoms in the prototype cell:
      do iat=1,nat
        read (ii1,*,err=804) (coor0(ii,iat),ii=1,3), 
     .                       (ww(ityp,iat),ityp=1,ntyp)
C                     assume fractional coordinates (in units of tau)
C                     ww are weights with which this site 
C                     can be occupied by different types.
        wsum = 0.d0
        do ityp=1,ntyp
          wsum = wsum + ww(ityp,iat)
        enddo
        if (abs(wsum-1.d0).gt.small) then
          write (6,*) ' For atom Nr.',ityp,
     .    ': weights does not sum to one; check!'
          stop
        endif
        
      enddo
      close (ii1)
C --- recover integer number of atoms of each type in supercell
      call expand(nat,coor0,mtau,nmult,coor1,norig)
      write(io1,"(2i4)") nat*nmult,1
      do iat=1,nat*nmult
        coor2(:,iat) = tau(:,1)*coor1(1,iat) +
     +                 tau(:,2)*coor1(2,iat) +
     +                 tau(:,3)*coor1(3,iat) 
        write(io1,301) norig(iat), (coor2(ii,iat),ii=1,3)
  301 format (i4, 2x,3f12.6)
      enddo


C     read (ii1,*) nsym
C --- read symmetry transformation matrices, row by row
C     do iat=1,nsym
C       read (ii1,*) ((mat(ii,jj,isym),jj=1,3),ii=1,3)
C     enddo
       
      close (io1)
      stop

  801 print *,' Error opening old formatted file ',ii1nam
      stop
  802 print *,' Error reading lattice vectors '
      stop
  803 print *,' Error reading supercell spanning vectors '
      stop
  804 print *,' Error reading coordinates for atom ',
     .        ' iat=',iat,'  of nat=',nat
      stop
      end
