C...............................................................
C
      subroutine makefc(nat,iz,nn1,nn2,
     .                 iat11,iat12,iat13,iat21,iat22,iat23,
     .                 coor11,coor12,coor13,coor21,coor22,coor23,fc)
C
C     using lists of 1st and 2d neighbors, update the fc matrix
C     based on heuristic rules.
C
C     Input:
C       nat          : number of atoms
C       iz(1..nat)   :  Z-values
C       nn1     : number of pairs of nearest neighbors (NN)
C       nn2     : number of pairs of next-nearest neighbors (NNN)
C       iat11(1..nn1):  first atom in NN pair
C       iat12(1..nn1):  second atom in NN pair
C       coor11(1..3,1..nn1):  coordinates of first atom in NN pair
C       coor12(1..3,1..nn1):  coordinates of second atom in NN pair
C                             (probably translated)
C       iat13(1..nn1):  the third (external) atom found for NN pair
C       coor13(1..3,1..n1):  its coordinates
C       iat21(1..nn2):  first atom in NNN pair
C       iat22(1..nn2):  second atom in NNN pair
C       coor21(1..3,1..nn2):  coordinates of first atom in NNN pair
C       coor22(1..3,1..nn2):  coordinates of second atom in NNN pair
C                             (probably translated)
C       iat23(1..nn2):  the third (middle) atom found for NNN pair
C       coor23(1..3,1..n2):  its coordinates
C     Output:
C       fc(1..3,1..nat,1..3,1..nat) : force constant matrix
C
      implicit none
      integer nat,io1,nn1,nn2,ip1,ip2,iat1,iat2,iat3,ix,jx,
     .        nz1,nz2,nz3
      parameter (io1 = 14)
      integer iz(nat),iat11(nn1),iat12(nn1),iat13(nn1),
     .                iat21(nn2),iat22(nn2),iat23(nn2)
      double precision coor11(3,nn1),coor12(3,nn1),coor13(3,nn1),
     .                 coor21(3,nn2),coor22(3,nn2),coor23(3,nn2),
     .                 fc(3,nat,3,nat),fcb(6,6),udiag(6),uright(6,6),
     .                 coor1(3),coor2(3),coor3(3)
      external bloc1n ! ,bloc2n

      fc(:,:,:,:) = 0.d0

C     Update blocks due to nearest neighbors:
      open (io1,file='FC_nn',form='FORMATTED',status='unknown')
      do ip1 = 1,nn1
        iat1 = iat11(ip1)
        iat2 = iat12(ip1)
        iat3 = iat13(ip1)
        nz1  = iz(iat1)
        nz2  = iz(iat2)
        coor1(:) = coor11(:,ip1)
        coor2(:) = coor12(:,ip1)
        coor3(:) = coor13(:,ip1)
        call bloc1n(nz1,nz2,coor1,coor2,coor3,udiag,uright,fcb)
C --- reconstruct FC similarly to how it was decomposed in fcmat.f :
        do ix=1,3
        do jx=1,3
          fc(jx,iat2,ix,iat1) = fcb(ix,3+jx)
          fc(ix,iat1,jx,iat2) = fcb(ix,3+jx)
          fc(jx,iat1,ix,iat2) = fcb(3+ix,jx)
          fc(ix,iat2,jx,iat1) = fcb(3+ix,jx)
        enddo
        enddo
 
        write (io1,"(/,' --- from atom ',i4,' at ( ',3f11.5,
     .              ' )')") iat1, coor1
        write (io1,"(7x,'to atom ',i4,' at ( ',3f11.5,
     .              ' )')") iat2, coor2
        write (io1,'("  Eigenvalues fit: ",6f10.6)')
     .                    (udiag(ix),ix=1,6)
        write (io1,'("  Eigenvectors from the heuristics:")')
        do jx=1,6
          write(14,'(i3,":",6f12.8)') jx,(uright(jx,ix),ix=1,6)
        enddo
        write (io1,'("  Reconstructed matrix of force constants:")')
        do ix=1,6
          write(io1,'(1p6e12.4)') (fcb(ix,jx),jx=1,6)
        enddo

      enddo
      close (io1)

C     Update blocks due to next nearest neighbors:
      open (io1,file='FC_nnn',form='FORMATTED',status='unknown')
      do ip2 = 1,nn2
        iat1 = iat21(ip2)
        iat2 = iat22(ip2)
        iat3 = iat23(ip2)
        nz1  = iz(iat1)
        nz2  = iz(iat2)
        nz3  = iz(iat3)
        coor1(:) = coor21(:,ip2)
        coor2(:) = coor22(:,ip2)
        coor3(:) = coor23(:,ip2)
        call bloc2n(nz1,nz2,nz3,coor1,coor2,coor3,udiag,uright,fcb)
C --- reconstruct FC similarly to how it was decomposed in fcmat.f :
        do ix=1,3
        do jx=1,3
          fc(jx,iat2,ix,iat1) = fcb(ix,3+jx)
          fc(ix,iat1,jx,iat2) = fcb(ix,3+jx)
          fc(jx,iat1,ix,iat2) = fcb(3+ix,jx)
          fc(ix,iat2,jx,iat1) = fcb(3+ix,jx)
        enddo
        enddo

        write (io1,"(/,' --- from atom ',i4,' at ( ',3f11.5,
     .              ' )')") iat1, coor1
        write (io1,"(7x,'to atom ',i4,' at ( ',3f11.5,
     .              ' )')") iat2, coor2
        write (io1,'("  Eigenvalues fit: ",6f10.6)')
     .                    (udiag(ix),ix=1,6)
        write (io1,'("  Eigenvectors from the heuristics:")')
        do jx=1,6
          write(14,'(i3,":",6f12.8)') jx,(uright(jx,ix),ix=1,6)
        enddo
        write (io1,'("  Reconstructed matrix of force constants:")')
        do ix=1,6
          write(io1,'(1p6e12.4)') (fcb(ix,jx),jx=1,6)
        enddo

      enddo
      close (io1)

      open (17,file='TestONsite',status='unknown')
C   reconstruct on-site terms from the acoustic sum rule:
C   FC(iat,iat) = - sum_iat FC(iat,jat)
      do iat1=1,nat
        fc(:,iat1,:,iat1) = 0.d0 
        do iat2=1,nat
          write (17,401) iat1,iat2
  401 format(' ---- Try iat1=',i3,'  iat2=',i3)
          if (iat2.eq.iat1) cycle
          do ix=1,3
          do jx=1,3
            fc(ix,iat1,jx,iat1) = fc(ix,iat1,jx,iat1) 
     .     - 0.5*( fc(ix,iat1,jx,iat2) + fc(jx,iat2,ix,iat1) )
          enddo
          enddo
          write(17,402)
          do ix=1,3
            write(17,403) (fc(ix,iat1,jx,iat2),jx=1,3),
     .                    (fc(ix,iat1,jx,iat1),jx=1,3)
  402  format('  Subtract block iat2=',i2,'  Resulting for iat1=',i2)
  403  format(1p3e11.3,5x,3e11.3)
          enddo
        enddo
      enddo
      close (17)

      return        
      end
