C
C...............................................................
C
      subroutine read_sym(ii3,nat,cosym)
C
C     reads the symmetry coefficients ii3 (free format),
C     skips the comment lines (which might be anywhere in the file,
C     beginning with '#' or '!' ,
C     checks that exactly three coefficients (X,Y,Z)
C     are provided for exactly 'nat' atoms.
C
C
      implicit none
      integer ii3,nat,iat,ii,iline,nat1
      double precision coefs(3),cosym(3,nat)
      character line*120
C
      iline=1
      nat1=0
  101 read(ii3,'(A)',err=801,end=802) line
C     write (6,*) '  iline=',iline,',  line --->',line,'<---'
      if ((line(1:1).eq.'#').or.(line(1:1).eq.'!')) then
C       a comment line; ignore
        iline=iline+1
        goto 101
      else
C       write (6,*) ' line -->',line,'<--'
        read (line,*,err=803,end=804) coefs
        nat1=nat1+1
        if (nat1.gt.nat) then
          write (6,*) ' Coefs read beyond the expected Nr. of atoms (',
     .    nat,' ) :'
C         write (6,*) ' iline,nat1,coefs=',iline,nat1,coefs
C         ... go on reading till end of file, without storing the coefficients
          goto 101
        else
          cosym(:,nat1)=coefs(:)
        endif
        iline=iline+1
        goto 101
      endif 
  801 write (6,*) ' Error reading from line Nr.',iline,
     .            ' of the symmetry file'
      stop
  802 if (nat1.eq.nat) then
C       do iat = 1,nat
C         write (6,'(i3,3f6.3)') iat,(cosym(ii,iat),ii=1,3)
C       enddo
        return    !  a regular return; the number of coefficients matches.
      else
        write (6,*) ' End of symmetry file: read coefficients for ',
     .               nat1,' atoms; expected for nat=',nat
        stop
      endif
  803 write (6,*) ' Error reading coefficients from line Nr.',iline,
     .            ' of the symmetry file'
      stop
  804 write (6,*) ' Unexpected end of record when reading coeffs ',
     .            ' from line Nr. ',iline,'  of the symmetry file'
      stop

      end
C
C...............................................................
