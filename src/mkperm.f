C
      subroutine mkperm(nat1,nat2,ityp,icod,nat0,sele,
     .                  cc_ang,coor_ang,mper)
C
C     Constructs the permutation matrix mper(1..nat2,1..nat2), showing how
C     the atoms in the supercell 1..nat2 are related (equivalent)
C     to inequivalent ("selected") ones, nat0(1) through nat0(nat1).
C     If iat1, the first index of mper(iat1,iat2),
C     is among the "inequivalent" ("selected") ones,
C     iat2 simply runs through all atoms of the supercell, 1 .. nat2.
C     If iat1 is NOT a "selected" one, the diagonal element mper(iat1,iat1)
C     gives the "selected" atom to which iat1 is equivalent.
C     Otherwise, mper(iat1,iat2) points to the atom in the supercell
C     which structurally stands (with all its replicas, taking translations
C     of the supercell into account) exactly so to mper(iat1,iat1)
C     as iat2 stands to iat1.
C
C     Input:   nat1 : number of inequivalent ("selected") atoms in supercell
C              nat2 : full number of atoms in supercell
C              ityp(1..nat2) : atom types 
C              icod(1..nat2) : work array for sorting out atoms
C              nat0(1..nat1) : list of "selected" atoms (arbitrary order)
C              cc_ang(xyz,1:3)      : translation vectors of supercell
C              coor_ang(1:3,1:nat2) : coordinates of atoms in supercell
C
C     Output:  mper(1..nat2,1..nat2) : permutation matrix.
C             
      implicit none
      integer nat1,nat2,nat0(nat1),ityp(nat2),mper(nat2,nat2),
     .        icod(nat2),mdisp,iat,jat,kat,idsp,jdsp,kdsp,iadd,
     .        n1dsp(3),n2dsp(3)
      integer io1
      logical sele(nat2)
      parameter (io1=15)
      parameter (mdisp=2) !   Nr. of translations when sorting out neighbors
      double precision cc_ang(3,3),coor_ang(3,nat2),
     .                 vol,dmax,dmin,dist,disp(3)

      open (io1,file='PERM_LOG',form='formatted')
! --- We'll try to identify atoms
!     (find a "selected" equivalent to all atoms of supercell)
!     without knowing the primitive cell vectors. For this,
!     we'll compare neighborhoods of atoms within a given max. distance.
!     Each neighborhood will be coded by a single integer number,
!     which will be then compared.
!     For trial distance, cubic root of cell volume seems to be a good measure:
      vol = cc_ang(1,1)*cc_ang(2,2)*cc_ang(3,3)+    
     .      cc_ang(1,2)*cc_ang(2,3)*cc_ang(3,1)+    
     .      cc_ang(1,3)*cc_ang(2,1)*cc_ang(3,2)-    
     .      cc_ang(1,1)*cc_ang(2,3)*cc_ang(3,2)-    
     .      cc_ang(1,2)*cc_ang(2,1)*cc_ang(3,3)-    
     .      cc_ang(1,3)*cc_ang(2,2)*cc_ang(3,1)    
      dmax = 2.0*(abs(vol))**(1./3.)
!     Take it *2, to better discriminate all atoms 
      dmin = 0.01     
!     write (io1,*) ' vol,dmax=',vol,dmax
      do iat = 1,nat2
!     write(io1,*)' ---- Start search for iat=',iat,' -----'
      icod(iat)=0
      do jat = 1,nat2
!       write(io1,*)'  ... Try jat=',jat,' -----'
! ---   allow some back/forth translations of supercell:
        do idsp=-mdisp,mdisp
        do jdsp=-mdisp,mdisp
        do kdsp=-mdisp,mdisp
          disp(:)=coor_ang(:,jat)-coor_ang(:,iat) +
     +            idsp*cc_ang(:,1)+jdsp*cc_ang(:,2)+kdsp*cc_ang(:,3)
          dist = sqrt(disp(1)**2+disp(2)**2+disp(3)**2)
          if ( (dist.gt.dmin).and.(dist.lt.dmax) ) then
! ---     count this neighbor and generate icode for it:
!         iadd =  nint(disp(1)*100.0)*29 +
!    +            nint(disp(2)*100.0)*113 +
!    +            nint(disp(3)*100.0)*229 +
!    +            ityp(jat)*349 
! --- The idea is, to generate for each atom pair a code which will be very
!     anisotropic (i.e., not likely subject to "accidental degeneracy"
!     in case of symmetric coordinates), that's why different weights
!     are assigned to Cartesian coordinates and +/- signs.
!     Real coordinates are rounded up to 0.01 Ang, e.g., 17.032 -> 1703,
!     in order to cope with integers which are easier to compare.
!     Different contributions to code are multiplied to different
!     prime numbers, to further reduce accidental degeneracies...
          iadd =  nint(abs(disp(1))*100.0)*29 +
     +            nint(abs(disp(2))*100.0)*113 +
     +            nint(abs(disp(3))*100.0)*229 +
     +            ityp(jat)*349 
          if (abs(disp(1)).gt.dmin) iadd = iadd+sign( 71,int(disp(1)))
          if (abs(disp(2)).gt.dmin) iadd = iadd+sign(349,int(disp(2)))
          if (abs(disp(3)).gt.dmin) iadd = iadd+sign(733,int(disp(3)))
!         write(io1,'(3f12.6,i20)') disp,iadd
          icod(iat) = icod(iat) + iadd
!         write (io1,301)idsp,jdsp,kdsp,iadd
          endif
        enddo
        enddo
        enddo
      enddo
      write (io1,302) iat,ityp(iat),icod(iat)
      enddo
! --- check consistency: icod of each "selected" atom must be unique
      if (nat1.gt.1) then
        do iat = 2,nat1
        do jat = 1,iat-1
          if (icod(nat0(jat)).eq.icod(nat0(iat))) then
            write (6,303) jat,nat0(jat),iat,nat0(iat)
            stop
          endif
        enddo
        enddo
      endif
! --- check consistency: every atom must have its "selected" equivalent
      mper(:,:) = 0 
!     write (io1,*) '  nat1=',nat1,'  nat2=',nat2
      l1:do jat = 1,nat2
!        write (io1,*) ' ---  jat=',jat,'   icod=',icod(jat)
         do iat = 1,nat1
           if (nat0(iat).eq.jat) then
!            jat is "selected" atom; fill its row in mper(:,:):
             do  kat=1,nat2
               mper(jat,kat) = kat
             enddo 
           cycle l1
           endif
         enddo
!      'jat' is not in the list of "selected" atoms; search which 
!      "selected" atom is equivalent to it: 
        do iat = 1,nat1
          if (icod(nat0(iat)).eq.icod(jat)) then
!           jat is EQUIVALENT to "selected" atom; assign diagonal element:
            mper(jat,jat) = nat0(iat)
! ---   add here the treatment of other elements !
          cycle l1
          endif
        enddo
! ---   jat is not "selected" atom nor equivalent to any of "selected" ones.
!       This means problem...
        write (6,304) jat
        stop
      enddo l1
! --- With all mper(jat,jat) now available,
!     update non-diagonal elements of mper()
!     write (io1,*)' Update non-diagonal elements of mper():' 
      l2:do jat = 1,nat2
!       write (io1,*)' -- jat=',jat 
        if (sele(jat)) cycle l2 ! For selected atoms, mper(jat,:) line is ready
        l3:do iat = 1,nat2
          if (iat.eq.jat) cycle l3 ! All diagonal elements are already done
!         mark position of iat relative to jat:
          n1dsp(:) = nint((coor_ang(:,iat)-coor_ang(:,jat))*100.0)
!         write (io1,*)'    -- iat=',iat,'  n1dsp=',n1dsp
! ---     search which atom stands in the same configuration to mper(jat,jat):
          do kat = 1,nat2
            do idsp=-mdisp,mdisp
            do jdsp=-mdisp,mdisp
            do kdsp=-mdisp,mdisp
              disp(:)=coor_ang(:,kat)-coor_ang(:,mper(jat,jat)) +
     +        idsp*cc_ang(:,1)+jdsp*cc_ang(:,2)+kdsp*cc_ang(:,3)
              n2dsp(:) = nint(disp(:)*100.0)
              if (all(n2dsp==n1dsp)) then ! compare integer (rounded) arrays
                mper(jat,iat) = kat
!               write (io1,*) '  match for kat=',kat,
!    .          ' ijk_dsp=',idsp,jdsp,kdsp,'  n2dsp=',n2dsp
! ---           An additional check: are atom types correct?
                if ( (ityp(jat).ne.ityp(mper(jat,jat))).or.
     .               (ityp(iat).ne.ityp(mper(jat,iat))) ) then
!                 write(6,305) jat,ityp(jat),
!    .                         mper(jat,jat),ityp(mper(jat,jat)),
!    .                         iat,ityp(iat),
!    .                         mper(jat,iat),ityp(mper(jat,iat))
                  stop
                endif
                cycle l3
              endif
            enddo 
            enddo 
            enddo 
          enddo 
! ---     Search over kat without success; a problem...
          write (6,306) jat,iat
          stop         
        enddo l3
      enddo l2
      write (io1,"(' ')")
      write (io1,"(' ++++ Permutation matrix ++++')")
      do jat = 1,nat2
        write(io1,'(20i4)') (mper(jat,iat),iat=1,nat2)
        if (nat2.gt.20) write(io1,"(' ')")
      enddo
      close (io1)
      return

  301 format('   i,j,kdisp=',3i3,'  iad=',i20)
  302 format(' +++ For iat=',i3,'  ityp=',i2,'  icod=',i20)
  303 format(' nat0(',i3,')=',i4,'  and nat0(',i3,')=',i4,
     .       ' seem equivalent! Either there is an error in the list',
     .       ' (the 3d line of input file), or a malfunction of',
     .       ' the algorithm assigning icod(). - Check subr. mkperm,',
     .       ' increase dmax and/or mdisp, or change algorithm...')
  304 format(' atom Nr.',i4,' from XV file seems an orphan:',/
     .       ' not in the list of 3d input line and not equivalent to',
     .       ' any from this list;',/' check completeness of list...')
  305 format(' WRONG atom types:',/
     .       ' ityp(jat=',i4,') =',i4,5x,
     .       ' ityp(mper(jat,jat)=',i4,') =',i4,/ 
     .       ' ityp(iat=',i4,') =',i4,5x,  
     .       ' ityp(mper(jat,iat)=',i4,') =',i4)
  306 format(' FAIL to find permutation matrix element for ',
     .       ' jat=',i4,' iat=',i4,' :'/
     .       ' No atom stands to mper(jat,jat)=',i4,' so as',
     .       ' iat stands to jat.')

      end
