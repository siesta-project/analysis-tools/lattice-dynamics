C
C    velcf,  a script to calculate the velocity correlation function
C            and its Fourier transform from molecular dynamics files 
C            (MD or ANI) written in SIESTA by subr. iomd
C
C       Written by Andrei Postnikov, May 2007   
C       postnikov@univ-metz.fr
C
      program velcf
      implicit none
      integer ii1,ii2,io1,io2,iat,nat,na,ialloc,ii,jj,mdmod,
     .        istep,nstep,idum,mdfirst,mdlast,mdstep,mdlim,
     .        ncorr,imd,jmd,kmd
      parameter (ii1=11,ii2=12,io1=13,io2=14)

      character inpfil*60,outfi1*60,outfi2*60,syslab*30,suffix*6
      logical varcel
      double precision b2ang,cc_bohr(3,3),cc_ang(3,3),cc_velo(3,3),
     .                 dummy,tstep,vcf,vcfc,vcfs,wfac,omega1,omega2
      parameter (b2ang=0.529177)   !  Bohr to Angstroem
      double precision,  allocatable :: veloc1(:),veloc2(:),vct(:)
      integer,           allocatable :: nz(:), ityp(:), nct(:)
      external test_md,test_ani,read_vmd,opnout
C
C     string manipulation functions in Fortran used below:
C     len_trimd(string): returns the length of string 
C                       without trailing blank characters,
C     trim(string)    : returns the string with railing blanks removed
      
      write (6,701,advance="no")
      read (5,*) syslab
C     inpfil = syslab(1:len_trim(syslab))//'.XV'
      inpfil = trim(syslab)//'.XV'
      open (ii1,file=inpfil,form='formatted',status='old',err=801)
      write (6,*) 'Found and opened: ',inpfil
C --  read in translation vectors, convert into Ang:
      do ii=1,3
        read  (ii1,702,end=803,err=803)  (cc_bohr(jj,ii),jj=1,3)
      enddo
      cc_ang = cc_bohr*b2ang
      read  (ii1,*,end=804,err=804)  nat
      allocate (veloc1(3*nat),STAT=ialloc)
      allocate (veloc2(3*nat),STAT=ialloc)
C --  velocities are allocated and used here in 1-dim arrays,
C     for the easiness of calculating their scalar products.
C     They are (hopefully) never addressed atom-resolved...
      allocate (nz(nat),STAT=ialloc)
      allocate (ityp(nat),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      do iat=1,nat
        read (ii1,703,end=805,err=805) ityp(iat), nz(iat), 
     .       (dummy,ii=1,3)   !  for coordinates which are of no interest here
      enddo
      close (ii1)
      write (6,*) ' with ',nat,'  atoms '
C --- finished with .XV
  103 write (6,704,advance="no")
      read (5,*) suffix
      inpfil = trim(syslab)//'.'//trim(suffix)
      if (trim(suffix).eq.'MD') then
        mdmod = 1
        open (ii2,file=inpfil,form='unformatted',status='old',err=806)
        write (6,*) 'Found and opened: ',inpfil
        call test_md(ii2,nat,varcel,nstep)
      elseif (trim(suffix).eq.'ANI') then
        mdmod = 2
        open (ii2,file=inpfil,form='formatted',status='old',err=801)
        write (6,*) 'Found and opened: ',inpfil
        call test_ani(ii2,nat,nstep)
      else
        write (6,*) ' Only MD or ANI allowed, try again:'
        goto 103
      endif
      write (6,705,advance="no")
      mdfirst = 0
      mdlast  = 0
      read (5,*) mdfirst, mdlast
      if (mdfirst.le.0) mdfirst = 1
      if (mdlast.le.0)  mdlast  = nstep
      if (mdlast.lt.mdfirst) mdlast = mdfirst
      mdstep = mdlast-mdfirst+1
      write (6,706) mdfirst,mdlast,mdstep
      write (6,707,advance="no") 
      read (5,*) ncorr
      if ((ncorr.le.0.).or.(ncorr.gt.mdstep-1)) ncorr=mdstep-1
C --- try to allocate memory for velocity correlation function:
      allocate (vct(0:ncorr),STAT=ialloc)  ! will be velocity auticorellation
      allocate (nct(0:ncorr),STAT=ialloc)  ! will be its normalization factor
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',ncorr,' steps.'
        stop
      endif
      write (6,708) ncorr,mdfirst

C --- Calculation of autocorrelation function
C     following an example from Chapter 6 of the book
C     by M.P.Allen and D.J.Tildesley
C     "Computer Simulation of Liquids", Clarendon, Oxford, 1987
C     ( ISBN 0-19-855645-4 )

      vct = 0.d0
      nct = 0
      write (6,709,advance="no") 
      read (5,*) tstep
C --- write (6,*) ' Constructing real-space velocity correlation function:')

      do imd = mdfirst,mdlast
C       store all velocities from the step  imd. It will be kept for a while
C       as a reference to calculate the correlations.
C       To get them (after previous readings), we must read the MD file anew:
        rewind (ii2)
        call read_vmd(ii2,nat,varcel,imd,veloc1)
C ---   Find number of forward steps from 'imd' on to trace correlations:
        mdlim = min(mdlast-imd,ncorr)  
        vct(0) = vct(0) + dot_product(veloc1,veloc1)
        nct(0) = nct(0) + 1
        do jmd = 1,mdlim
          call read_vmd(ii2,nat,varcel,1,veloc2)
          vct(jmd) = vct(jmd) + dot_product(veloc1,veloc2)
          nct(jmd) = nct(jmd) + 1
        enddo
      enddo
      vct = vct / nct

      outfi1 = trim(syslab)//'.VCT'
      call opnout(io1,outfi1)
      write(io1,901)
      do jmd = 0,ncorr
        write(io1,903) jmd,vct(jmd)
      enddo
      
C --- 

C --- open output file for Fourier transformed velocity correlation function:
      outfi2 = trim(syslab)//'.VCF'
      call opnout(io2,outfi2)
      write(io2,902)
C --- make slow Fourier transform (cosine; discrete):
C     frequency factor in the cosine argument:
      wfac = 4.0*atan(1.0)/(ncorr+1)
      do kmd = 0,ncorr
        vcfc = 0.d0
        vcfs = 0.d0
        do jmd = 0,ncorr
          vcfc = vcfc + vct(jmd)*cos(wfac*jmd*kmd)
          vcfs = vcfs + vct(jmd)*sin(wfac*jmd*kmd)
        enddo
        vcfc = vcfc * 2.0/(ncorr+1)
        vcfs = vcfs * 2.0/(ncorr+1)
        vcf = sqrt(vcfc*vcfc + vcfs*vcfs)
!!#       Frequency in THz:
        omega1 = 500.0/((ncorr+1)*tstep)*kmd
!!#       Frequency in cm-1:
        omega2 = omega1 * 33.356
        write(io2,903) kmd,omega1,omega2,vcfc,vcfs,vcf
      enddo

      close (io1) 
      close (io2) 
      deallocate (veloc1,veloc2,nz,ityp,vct,nct)

      stop

  701 format(" Specify  SystemLabel (or 'siesta' if none): ")
  702 format(3x,3f18.9)
  703 format(i3,i6,3f18.9)
  704 format(' Suffix of molecular dynamics file (MD or ANI): ')
  705 format(' You may wish to retain only some part of this MD',
     .       ' history for subsequent analysis.',/,
     .       ' Specify two numbers MDfirst, MDlast -',
     .       ' or 0 for any of them as default : ')
  706 format(" OK, I'll keep steps Nr.",i6,',through 'i6,
     .       ' ( total',i6,' )')   
  707 format(' You may wish to limit the maximal correlation length',/,
     .       ' to be calculated. This can make sense',
     .       ' in very long runs,',/,' where the correlations', 
     .       ' fall down much faster. Give the max. number of steps',/,
     .       ' to cutoff correlations (by default no cutoff) : ')
  708 format(' Allocated memory for ',i8,' steps',/,
     .       ' of the velocity autocorrelation function,',/,
     .       ' starting from step ',i8,' of the MD run.')
  709 format(' Simulation time step in femtaseconds: ')
  901 format('# Velocity autocorrelation function, time domain', /
     .       '#   step     time(fs)    function')
  902 format('# Velocity autocorrelation function, frequency domain', /
     .       '#   step     frequency(THz)  (cm-1)  function')
  903 format(i8,2f10.3,3f16.10)

  801 write (6,*) ' Error opening file ',
     .            trim(inpfil),' as old formatted'
      stop
  803 write (6,*) ' End/Error reading XV for cell vector ',ii
      stop
  804 write (6,*) ' End/Error reading XV for number of atoms line'
      stop
  805 write (6,*) ' End/Error reading XV for atom number ',iat
      stop
  806 write (6,*) ' Error opening file ',
     .            trim(inpfil),' as old unformatted'
      stop

      end
