C...............................................................
C
      subroutine find3(nn1,nn2,
     .                 iat11,iat12,iat13,iat21,iat22,iat23,
     .                 coor11,coor12,coor13,coor21,coor22,coor23)
C
C     for each pair of 1st and 2d neighbors, finds the third atom
C     which will fix the local system. 
C     For nearest neighbors in the A-B pair, this is either
C     that nearest neighbor to A which is closest to B, or
C     the nearest neighbor to B which is closest to A.
C     For second nearest neighbors A...B, this is the atom C which
C     is first neighbor to both A and B, and specifically (if there are 
C     several of such kind) that which is closest to both, i.e. 
C     (A-C)**2 + (B-C)**2 is minimal. Hopefully this will set
C     the unique link A-C-B.
C
C     Input:
C       nn1     : number of pairs of nearest neighbors (NN)
C       nn2     : number of pairs of next-nearest neighbors (NNN)
C       iat11(1..nn1):  first atom in NN pair
C       iat12(1..nn1):  second atom in NN pair
C       coor11(1..3,1..nn1):  coordinates of first atom in NN pair
C       coor12(1..3,1..nn1):  coordinates of second atom in NN pair
C                             (probably translated)
C       iat21(1..nn2):  first atom in NNN pair
C       iat22(1..nn2):  second atom in NNN pair
C       coor21(1..3,1..nn2):  coordinates of first atom in NNN pair
C       coor22(1..3,1..nn2):  coordinates of second atom in NNN pair
C                             (probably translated)
C     Output:
C       iat13(1..nn1):  the third (external) atom found for NN pair
C       coor13(1..3,1..n1):  its coordinates
C       iat23(1..nn2):  the third (middle) atom found for NNN pair
C       coor23(1..3,1..n2):  its coordinates
C
C     Note:
C       coor11 and coor12 (smilarly coor21 and coor22) are "compact",
C       after having possibly applied translations to the second atom 
C       of a pair. Now, while joining the pairs for finding the third atom,
C       it should be taken into account that two pairs are 
C       possibly separated by a lattice translation. A correction
C       is undertaken everywhere below so that to assign coor13 
C       in a "compact" manner to coor11 and coor12; similarly
C       for the second neighbours
C
      implicit none
      integer nn1,nn2,ip1,ip2,io1,ix
      parameter (io1 = 14)
      integer iat11(nn1),iat12(nn1),iat13(nn1),
     .        iat21(nn2),iat22(nn2),iat23(nn2)
      double precision coor11(3,nn1),coor12(3,nn1),coor13(3,nn1),
     .                 coor21(3,nn2),coor22(3,nn2),coor23(3,nn2),
     .                 trans(3),dist,dstmin,huge
      parameter (huge = 10.d+8)

      open (io1,file='NGBH',form='FORMATTED',status='unknown')
      write (io1, *) ' n1,nn2=',nn1,nn2
      write (io1,*) ' --- Nearest neighbors ---'
C --- For each nearest-neighbors pair, find one extra atom which is 
C     nearest neighbor to one atom of the pair, and the closest one
C     to the other atom of the pair. 
C     It will be used to set up the local system
      do ip1 = 1,nn1
        dstmin = huge
        do ip2 = 1,nn1
          if (ip2.eq.ip1) cycle   !  try all other neighbor 
          if ( iat11(ip2).eq.iat11(ip1) ) then
C           in principle no translation between two first atoms, 
C           but we add it for generality, even if it will be zero:
            trans(:)=coor11(:,ip2)-coor11(:,ip1)
            dist = (coor12(1,ip2)-trans(1) - coor12(1,ip1))**2 +
     +             (coor12(2,ip2)-trans(2) - coor12(2,ip1))**2 +
     +             (coor12(3,ip2)-trans(3) - coor12(3,ip1))**2 
            if (dist.lt.dstmin) then
              iat13(ip1)    = iat12(ip2)
              coor13(:,ip1) = coor12(:,ip2)-trans(:)
              dstmin = dist
            endif
          elseif ( iat12(ip2).eq.iat11(ip1) ) then
C           the second pair might be translated;
C           check if correction/translation is needed:
            trans(:)=coor12(:,ip2)-coor11(:,ip1)
            dist = (coor11(1,ip2)-trans(1) - coor12(1,ip1))**2 +
     +             (coor11(2,ip2)-trans(2) - coor12(2,ip1))**2 +
     +             (coor11(3,ip2)-trans(3) - coor12(3,ip1))**2 
            if (dist.lt.dstmin) then
              iat13(ip1)    = iat11(ip2)
              coor13(:,ip1) = coor11(:,ip2)-trans(:)
               dstmin = dist
            endif
          elseif ( iat11(ip2).eq.iat12(ip1) ) then
C           the second pair might be translated;
C           check if correction/translation is needed:
            trans(:)=coor11(:,ip2)-coor12(:,ip1)
            dist = (coor12(1,ip2)-trans(1) - coor11(1,ip1))**2 +
     +             (coor12(2,ip2)-trans(2) - coor11(2,ip1))**2 +
     +             (coor12(3,ip2)-trans(3) - coor11(3,ip1))**2 
            if (dist.lt.dstmin) then
              iat13(ip1)    = iat12(ip2)
              coor13(:,ip1) = coor12(:,ip2)-trans(:)
              dstmin = dist
            endif
          elseif ( iat12(ip2).eq.iat12(ip1) ) then
C           the second pair might be translated;
C           check if correction/translation is needed:
            trans(:)=coor12(:,ip2)-coor12(:,ip1)
            dist = (coor11(1,ip2)-trans(1) - coor11(1,ip1))**2 +
     +             (coor11(2,ip2)-trans(2) - coor11(2,ip1))**2 +
     +             (coor11(3,ip2)-trans(3) - coor11(3,ip1))**2 
            if (dist.lt.dstmin) then
              iat13(ip1)    = iat11(ip2)
              coor13(:,ip1) = coor11(:,ip2)-trans(:)
              dstmin = dist
            endif
          endif
        enddo
        write (io1,202) iat11(ip1),(coor11(ix,ip1),ix=1,3),
     .                  iat12(ip1),(coor12(ix,ip1),ix=1,3),
     .                  iat13(ip1),(coor13(ix,ip1),ix=1,3)
      enddo

      write (io1,*) '  '
      write (io1,*) ' --- Next-nearest neighbors ---'
C --- For each second-next-neighbors pair, find the atom which is 
C     nearest neighbor to both, and moreover the closest one
C     to both atoms in the pair. It will be used to set up the local system.
      do ip2 = 1,nn2
        dstmin = huge
        do ip1 = 1,nn1
          if (iat11(ip1).eq.iat21(ip2)) then
C           the second pair might be translated;
C           check if correction/translation is needed:
            trans(:)=coor11(:,ip1)-coor21(:,ip2)
            dist = (coor12(1,ip1)-trans(1) - coor21(1,ip2))**2 +
     +             (coor12(2,ip1)-trans(2) - coor21(2,ip2))**2 +
     +             (coor12(3,ip1)-trans(3) - coor21(3,ip2))**2 +
     +             (coor12(1,ip1)-trans(1) - coor22(1,ip2))**2 +
     +             (coor12(2,ip1)-trans(2) - coor22(2,ip2))**2 +
     +             (coor12(3,ip1)-trans(3) - coor22(3,ip2))**2 
            if (dist.lt.dstmin) then
              iat23(ip2)    = iat12(ip1)
              coor23(:,ip2) = coor12(:,ip1)-trans(:)
              dstmin = dist
            endif
          elseif (iat12(ip1).eq.iat21(ip2)) then
C           check if correction/translation is needed:
            trans(:)=coor12(:,ip1)-coor21(:,ip2)
            dist = (coor11(1,ip1)-trans(1) - coor21(1,ip2))**2 +
     +             (coor11(2,ip1)-trans(2) - coor21(2,ip2))**2 +
     +             (coor11(3,ip1)-trans(3) - coor21(3,ip2))**2 +
     +             (coor11(1,ip1)-trans(1) - coor22(1,ip2))**2 +
     +             (coor11(2,ip1)-trans(2) - coor22(2,ip2))**2 +
     +             (coor11(3,ip1)-trans(3) - coor22(3,ip2))**2 
            if (dist.lt.dstmin) then
              iat23(ip2)    = iat11(ip1)
              coor23(:,ip2) = coor11(:,ip1)-trans(:)
              dstmin = dist
            endif
          endif
        enddo
        write (io1,203) iat21(ip2),(coor21(ix,ip2),ix=1,3),
     .                  iat22(ip2),(coor22(ix,ip2),ix=1,3),
     .                  iat23(ip2),(coor23(ix,ip2),ix=1,3)
      enddo
      close (io1)
      return
  202 format(i4,'(',3f8.3,' ) and',i4,'(',3f8.3,
     .                    ' ), ext:',i4,'(',3f8.3,')')
  203 format(i4,'(',3f8.3,' ) and',i4,'(',3f8.3,
     .                    ' ), mid:',i4,'(',3f8.3,')')

      end
