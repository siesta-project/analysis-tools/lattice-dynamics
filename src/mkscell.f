      program mkscell

      implicit none
      character ii1nam*60
      integer nat,ii1,io1,ii,nat,iat,ntyp,ityp,nsym,isym,ialloc,
     .        mat(3,3,48),mtau(3,3)
      parameter (ii1=1,io1=12)
      double precision tau(3,3)
      double precision, allocatable :: coor(:,:),ww(:,:)

      write (6,"(' Input file :')",advance="no")
      read (5,*) ii1nam
      open (ii1,file=ii1nam,form='formatted',status='old',err=801)
C --- read vectors of the prototype (single) cell:
      read (ii1,*) (tau(ii,1),ii=1,3)
      read (ii1,*) (tau(ii,2),ii=1,3)
      read (ii1,*) (tau(ii,3),ii=1,3)
C --- read supercell multiplication vectors:
      read (ii1,*) (mtau(ii,1),ii=1,3)
      read (ii1,*) (mtau(ii,2),ii=1,3)
      read (ii1,*) (mtau(ii,3),ii=1,3)
C --- read number of atoms and species in the prototype cell:
      read (ii1,*) nat,ntyp
      allocate (coor(3,nat),ww(ntyp,nat),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
C --- read atoms in the prototype cell:
      do iat=1,nat
        read (ii1,*) (coor(ii,iat),ii=1,3), (ww(ityp,iat),ityp=1,ntyp)
C                     ww are weights with which this site 
C                     can be occupied by different types.
        wsum = 0.d0
        do ityp=1,ntyp
          wsum = wsum + ww(ityp,iat)
        enddo
        if (abs(wsum-1.d0).lt.small) then
          write (6,*) ' For atom Nr.',ityp,
     .    ': weights does not sum to one; check!'
          stop
        endif
      enddo
      read (ii1,*) nsym
C --- read symmetry transformation matrices, row by row
      do iat=1,nsym
        read (ii1,*) ((mat(ii,jj,isym),jj=1,3),ii=1,3)
      enddo
       
      stop
      end
