      program fcfit
C
C       reads SIESTA XV file and uses heuristic rules 
C       (programed ad hoc for each pair of atoms)
C       to recover the FC file, valid for the further use with vibrator.
C       The idea is that the force constants are essentially dependent
C       on 1st + 2d neighbors (bond lenghts, angles)
C       which is certainly an approximation, but hopefully
C       a poor man's substitution to thue frozen phonon calculation.
C       The heuristics tuned for: (Zn,Be)Se
C       
C       Written by A. Postnikov, apostnik@uos.de
C
      implicit none
      integer ii1,io1
      parameter (ii1=11,io1=14)
      integer ialloc,nat,iat,jat,nn1,nn2,ipm,ix,jx
      integer, allocatable :: ityp(:),iz(:),nna(:),
     .                        iat11(:),iat12(:),iat13(:),
     .                        iat21(:),iat22(:),iat23(:)
      character inpfil*60,outfil*60,syslab*30
      double precision cc_ang(3,3),dmax1,dmax2
      double precision, allocatable :: mass(:),coor_ang(:,:),
     .                                 fc(:,:,:,:),
     .                  coor11(:,:),coor12(:,:),coor13(:,:),
     .                  coor21(:,:),coor22(:,:),coor23(:,:)
      character*2, allocatable :: label(:)
      external test_xv,read_xv,find3,makefc,opnout,sortnn
C
C --- read lattice vectors and atom positions from the .XV file:
      write (6,"(' Specify SystemLabel of .XV file: ')",
     .       advance="no")
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.XV'
      open (ii1,file=inpfil,form='formatted',status='old',err=801)
      call test_xv(ii1,nat)
      allocate (ityp(nat),iz(nat))
      allocate (label(nat))
      allocate (mass(nat),coor_ang(1:3,1:nat))
      allocate (fc(1:3,1:nat,1:3,1:nat),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      call read_xv(ii1,nat,ityp,iz,cc_ang,mass,label,coor_ang)
      close (ii1)

  101 write (6,*) ' Enter the limiting distance',
     .            ' to include all nearest neighbors (in Ang):'
      read (5,*) dmax1
      if (dmax1.le.0.) goto 101
  102 write (6,*) ' Enter the limiting distance',
     .            ' to include all next-nearest neighbors (in Ang):'
      read (5,*) dmax2
      if (dmax2.le.dmax1) goto 102
C
      open (io1,file='LOG',form='FORMATTED',status='unknown')
C --- call sortnn(0,...  returns nn1,nn2 for alocating arrays
      call sortnn(0,nn1,nn2,nat,
     .            cc_ang,coor_ang,dmax1,dmax2,
     .            iat11,iat12,iat21,iat22,
     .            coor11,coor12,coor21,coor22)

      allocate (iat11(1:nn1),iat12(1:nn1),iat13(1:nn1))
      allocate (coor11(1:3,1:nn1),coor12(1:3,1:nn1),coor13(1:3,1:nn1))
      allocate (iat21(1:nn2),iat22(1:nn2),iat23(1:nn2))
      allocate (coor21(1:3,1:nn2),coor22(1:3,1:nn2),coor23(1:3,1:nn2),
     .          STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nn1,
     .       ' 1st neighbor pairs and ',nn2,' 2d neighbor pairs.'
        stop
      endif
C --- call sortnn(1,... find types and coordinates of atoms
C                 in pairs of 1st and 2d neighbors
      call sortnn(1,nn1,nn2,nat,
     .            cc_ang,coor_ang,dmax1,dmax2,
     .            iat11,iat12,iat21,iat22,
     .            coor11,coor12,coor21,coor22)

      call find3(nn1,nn2,iat11,iat12,iat13,iat21,iat22,iat23,
     .           coor11,coor12,coor13,coor21,coor22,coor23)

      call makefc(nat,iz,nn1,nn2,
     .            iat11,iat12,iat13,iat21,iat22,iat23,
     .            coor11,coor12,coor13,coor21,coor22,coor23,fc)
      
C --- Write down in the standard Siesta format
      outfil = syslab(1:len_trim(syslab))//'-fit.FC'
      call opnout(io1,outfil)
      write (io1,"('Force constants matrix, parametrized')")    
      do iat=1,nat   !   atom which is displaced
        do ix =1,3     !   Cartesian displacements
        do ipm =1,2    !   displace - and +
        do jat=1,nat   !   atom at which force is induced
          write (io1,201) (fc(jx,jat,ix,iat),jx=1,3)
        enddo
        enddo
        enddo
      enddo
      close (io1)
      
      stop

  201 format (3f15.7)
  801 write (6,*) ' Error opening file ',
     .            inpfil(1:len_trim(inpfil)),' as old formatted'
      stop

      end
