C...............................................................
C
      subroutine bloc1n(nz1,nz2,coor1,coor2,coor3,fcb)
C
C     makes (6*6) block of force constants
C     based on heuristic rules for nearest neighbors.
C
C     Input:
C       nz1          : Z-value for 1st atom
C       nz1          : Z-value for 2d  atom
C       coor1        : coordinates of 1st atom
C       coor2        : coordinates of 2d  atom
C       coor3        : coordinates of 3d (added) atom
C     Output:
C       fcb(1..6,1..6) : block of force constant matrix
C
      implicit none
      integer nz1,nz2
      double precision coor1(3),coor2(3),coor3(3),fcb(6,6)

      return
      end
C
C...............................................................
C
      subroutine bloc2n(nz1,nz2,coor1,coor2,coor3,fcb)
C
C     makes (6*6) block of force constants
C     based on heuristic rules for next nearest neighbors.
C
C     Input:
C       nz1          : Z-value for 1st atom
C       nz1          : Z-value for 2d  atom
C       coor1        : coordinates of 1st atom
C       coor2        : coordinates of 2d  atom
C       coor3        : coordinates of 3d (added) atom
C     Output:
C       fcb(1..6,1..6) : block of force constant matrix
C
      implicit none
      integer nz1,nz2
      double precision coor1(3),coor2(3),coor3(3),fcb(6,6)

      return
      end
C
