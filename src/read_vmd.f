C...............................................................
C
      subroutine read_vmd(ii2,nat,varcel,nstep,veloc)        
C
C     reads from ii2 (MD file) the data of 'nstep' MD steps,
C     returns the velocities (of all 'nat' atoms)
C     from the last step.
C
C     MD file is written in iomd.f as follows (for formt = F ):
C       write(iupos) istep, xa(1..3,1..nat), va(1..3,1..nat)
C       if ( varcel ) write(iupos) cell(1..3,1..3), vcell(1..3,1..3)
C
      implicit none
      integer ii2,nat,nstep,istep,istep1,ii
      logical varcel
      double precision dummy,veloc(3*nat)
C 
C     As in the program velcf.f, we define and address velocities
C     on a 1-dim. array of the size 3*nat. It make need change
C     (in this subroutine only) when using formatted input/output

      if (nstep.eq.0) return
      if (nstep.lt.0) then
        write (6,*) ' Stop in read_velmd: negative nstep =',nstep
        stop
      endif

      do istep = 1,nstep
        read (ii2,err=801,end=803) istep1,(dummy,ii=1,3*nat),veloc
        if (varcel) read (ii2,err=802,end=803) (dummy,ii=1,6*nat)
      enddo
C     We do not check the consistency of the step number,
C     because this subroutine is supposed to make as many readings
C     as requested. The checks have been done earlier in subr. test_md.
C     However, we check for general consistency and end of file.
C
C     In normal regime, velocities are overwritten on each step, 
C     and those from the last step are returned:

      return

  801 continue
      write (6,*) ' Read_velmd: error reading coord. or velocities ',
     .            ' for istep=',istep
      stop
  802 continue
      write (6,*) ' Read_velmd: error reading variable cell ',
     .            ' for istep=',istep
      stop
  803 continue
      write (6,*) ' Read_velmd: read attempt past EOF ',
     .            ' for istep=',istep
      stop
      
      end 
