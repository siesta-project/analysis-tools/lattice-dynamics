      program fcxsf
C
C       reads SIESTA FC file,
C       diagonalizes the dynamical matrix blockwise,
C       makes XSF input file (for XCrySDen) to vizualize diagonal elements
C       of the force constants for a selected pair of atom types,
C       within a selected output box.
C
C       Written by A. Postnikov, apostnik@uos.de
C
      implicit none
      integer ii1,ii2,io1,is1,is2,imod,nbox,ibox,jbox,jj
      parameter (ii1=11,ii2=12,io1=14,is1=17,is2=18)
      integer ialloc,ierr,iat,jat,nat,ix,jx,kx,ipm,iline,ityp1,ityp2,
     .         idisp,jdisp,kdisp,mdisp
      integer, allocatable :: ityp(:),iz(:),nna(:)

      character inpfil*60,outfil*60,syslab*30
      double precision cc_ang(3,3),cc_inv(3,3),dmin,dmax,
     .                 dc(3,3),uu(3,3),db(3,3),cart1(3),cart2(3),
     .                 dc2(6,6),uu2(6,6),db2(6,6),
     .                 dmid0(3),dmid1(3),dmid2(3),
     .                 vec0(3,3),vec1(3,3),vec2(3,3),scal,fac,
     .                 dist,xdisp,ydisp,zdisp,dx,dy,dz,fv1(6),fv2(6),
     .                 eval(6),obox(3),rbox(3,3),rinv(3,3)
      parameter (fac = 1.0d0)
      double precision, allocatable :: mass(:),coor_ang(:,:),
     .                                 fc(:,:,:,:,:)
      character*2, allocatable :: label(:)
      external test_xv,read_xv,rs,opnout,inver3,makebox,fillbox
C
C --- read lattice vectors and atom positions from the .XV file:
      write (6,"(' Specify SystemLabel of .XV and .FC files: ')",
     .       advance="no")
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.XV'
      open (ii1,file=inpfil,form='formatted',status='old',err=801)
      call test_xv(ii1,nat)
      allocate (ityp(nat),iz(nat))
      allocate (label(nat))
      allocate (mass(nat),coor_ang(1:3,1:nat))
      allocate (fc(1:3,1:nat,1:3,1:nat,1:2),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      call read_xv(ii1,nat,ityp,iz,cc_ang,mass,label,coor_ang)
      call inver3(cc_ang,cc_inv)
      close (ii1)
C
C --- read force constants from the .FC file:
C     write (6,"(' Specify SystemLabel of .FC file: ')",advance="no")
C     read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.FC'
      open (ii2,file=inpfil,form='formatted',status='old',err=801)
      iline = 0
      read (ii2,*)    
      do iat=1,nat   !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat   !   atom at which force is induced
          iline=iline + 1
          read (ii2,'(3f15.7)',err=301,end=302) 
     .          (fc(jx,jat,ix,iat,ipm),jx=1,3)
        enddo
      enddo
      enddo
      enddo
      close (ii2)
C
      write (6,"(' Two atom types for extracting force constants: ')",
     .       advance="no")
      read (5,*) ityp1,ityp2
   11 continue
      write (6,"(' Max and min distance (in Ang) between them: ')",
     .       advance="no")
      read (5,*) dmin,dmax
      if (dmin.lt.0..or.dmax.lt.0.) then
        write (6,*) '  dmax and dmin must not be negative'
        goto 11
      endif
      if (dmin.ge.dmax) then
        write (6,*) ' dmax must be larger than dmin'
        goto 11
      endif
C --- try all pairs of atoms of types ityp1 and ityp2
      outfil = syslab(1:len_trim(syslab))//'.FCxsf'
      call opnout(io1,outfil)
      write (io1,201) ityp1,ityp2,dmin,dmax
C --- set up and fill output box:
      call makebox(obox,rbox)
C --- invert the box vectors; will need it in a minute...
      call inver3(rbox,rinv)
C --- write selected atoms into two identical scratch files is1 and is2, 
C     to organize a double loop over them.
      open (is1,form='formatted',status='scratch')
      open (is2,form='formatted',status='scratch')
C     open (is1,file='tmpf1',form='formatted',status='scratch')
C     open (is2,file='tmpf2',form='formatted',status='scratch')
C     open (is1,file='tmpf1',form='formatted',status='unknown')
C     open (is2,file='tmpf2',form='formatted',status='unknown')
      call fillbox(is1,obox,rbox,rinv,cc_ang,nat,coor_ang,nbox)
      call fillbox(is2,obox,rbox,rinv,cc_ang,nat,coor_ang,nbox)
      write (6,*) ' The box contains ',nbox,' atoms.'
      if (nbox.gt.0) then    !  add atoms to XSF
        write (io1,'(a5)') 'ATOMS'
        rewind (is1)
        do ibox = 1,nbox
          read  (is1,*) iat,     (cart1(jj),jj=1,3)
          write (io1,'(i3,3f11.6)') iz(iat), (cart1(jj),jj=1,3)
          rewind (is2)
          if (ibox.gt.1) then   !   dummy read for jbox < ibox
            do jbox = 1,ibox-1
              read  (is2,*) jat,     (cart2(jj),jj=1,3)
            enddo
          endif
          do jbox = ibox,nbox
            read  (is2,*) jat,     (cart2(jj),jj=1,3)
            if (ityp(iat).eq.ityp1.and.ityp(jat).eq.ityp2) then
C             check distance
              dist = sqrt( (cart1(1)-cart2(1))**2 +
     +                     (cart1(2)-cart2(2))**2 +
     +                     (cart1(3)-cart2(3))**2 )
              if (dist.le.dmax.and.dist.ge.dmin) then
C ---           Keep this pair; calculate the point where to place the arrows:
                dmid0(:) = 0.5*cart1(:) + 0.5*cart2(:)
                dmid1(:) = 0.6*cart1(:) + 0.4*cart2(:)
                dmid2(:) = 0.4*cart1(:) + 0.6*cart2(:)
C ---           analyze force constants:
                if (iat.eq.jat) then  !  on-site 3*3 matrix
                  do ix=1,3
                  do jx=1,3
                    dc(ix,jx) = ( fc(jx,jat,ix,iat,1) +
     +                            fc(jx,jat,ix,iat,2) +    
     +                            fc(ix,iat,jx,jat,1) +
     +                            fc(ix,iat,jx,jat,2) )/4
                  enddo
                  enddo
C --- to diagonalize a symmetric matrix, call the RS sequence of EISPACK:
                  call rs(3,3,dc,eval,1,uu,fv1,fv2,ierr)
                  if (ierr.ne.0) then
                    print *,' ierr=',ierr,' from RS'
                    stop
                  endif
C --- Scale the eigenvectors by corresponding eigenvalues.
C     and plot them as +/- arrows, centered as a given atom:
                  do jx=1,3
C                   vec0(:,jx) = uu(:,jx)*eval(jx)*fac
                    vec0(:,jx) = uu(:,jx)*real(jx)*fac
                    write (io1,'("  X",3f11.6,2x,3f11.6,i4)') 
     .                    (dmid0(jj),jj=1,3),(vec0(ix,jx),ix=1,3),iat
C                   write (io1,'("  X",3f11.6,2x,3f11.6,i4)') 
C    .                    (dmid0(jj),jj=1,3),(-vec0(ix,jx),ix=1,3),iat
                  enddo
                else   !   --- iat.ne.jat: make off-site 6*6 matrix
                  dc2(:,:) = 0.d0
                  do ix=1,3
                  do jx=1,3
                    dc2(ix,3+jx) = ( fc(jx,jat,ix,iat,1) +
     +                               fc(jx,jat,ix,iat,2) +    
     +                               fc(ix,iat,jx,jat,1) +
     +                               fc(ix,iat,jx,jat,2) )/4  
                    dc2(3+ix,jx) = ( fc(jx,iat,ix,jat,1) +
     +                               fc(jx,iat,ix,jat,2) +    
     +                               fc(ix,jat,jx,iat,1) +
     +                               fc(ix,jat,jx,iat,2) )/4
                  enddo
                  enddo
                  call rs(6,6,dc2,eval,1,uu2,fv1,fv2,ierr)
                  if (ierr.ne.0) then
                    print *,' ierr=',ierr,' from RS'
                    stop
                  endif
C --- eigenvectors scaled by corresponding eigenvalues:
                  do jx=1,3
                    do ix=1,3
C                     vec1(ix,jx) = uu2(  ix,3+jx)*eval(3+jx)*fac
C                     vec2(ix,jx) = uu2(3+ix,3+jx)*eval(3+jx)*fac
C ---  scale eigenvectors simply by 1,2,3
                      vec1(ix,jx) = uu2(  ix,3+jx)*real(jx)*fac
                      vec2(ix,jx) = uu2(3+ix,3+jx)*real(jx)*fac
                    enddo
C --- Try to balance the errors nicely with respect to the line
C     connecting two atoms (so that they point outwards), 
C     inverting the corresponding pair of arrows if necessary:
                    scal = (vec1(1,jx)-vec2(1,jx))*(cart2(1)-cart1(1))+
     +                     (vec1(2,jx)-vec2(2,jx))*(cart2(2)-cart1(2))+
     +                     (vec1(3,jx)-vec2(3,jx))*(cart2(3)-cart1(3)) 
C                   if (scal.gt.0.) then
C                     do ix=1,3
C                       vec1(ix,jx) = -vec1(ix,jx)
C                       vec2(ix,jx) = -vec2(ix,jx)
C                     enddo
C                   endif
                    write (io1,'("  X",3f11.6,2x,3f11.6,2i4)') 
     .                    (dmid1(jj),jj=1,3),
     .                    (vec1(ix,jx),ix=1,3),iat,jat
                    write (io1,'("  X",3f11.6,2x,3f11.6,2i4)') 
     .                    (dmid2(jj),jj=1,3),
     .                    (vec2(ix,jx),ix=1,3),iat,jat
                  enddo
                endif    !   (iat.eq.jat)
              endif    !   (dist.le.dmax.and.dist.ge.dmin) 
            endif    !   (ityp(iat).eq.ityp1.and.ityp(jat).eq.ityp2) 
          enddo
        enddo
      else
        write (6,*) ' This is OK, just be warned that your XSF file',
     .              ' will have no ATOMS secton!'
      endif
      close (is1)
      close (is2)

      close (io1)
      stop
  301 continue
      print *,' Error reading FC file, line ',iline
      stop
  302 continue
      print *,' Unexpected end of FC file, iat,ix,ipm,jat=',
     .         iat,ix,ipm,jat     
      stop
  201 format('#',/,
     .       '#  Matrix of force constants between types ',
     .        i3,'  and ',i3,/
     .       '#  distance from ',f9.4,' to ',f9.4'  Angstroem:',/
     .       '# ')
  202 format('#',/,
     .       '#  Principal elements of force constants between types ',
     .        i3,'  and ',i3,/
     .       '#  distance from ',f9.4,' to ',f9.4' Angstroem:',/
     .       '#  iat jat   dist',9x,15('-'),' FC diag. ',15('-'))
  203 format(1x,2i4,f9.4,3x,1p9e11.3)
  204 format(1x,2i4,f9.4,3x,6f15.8)

  801 write (6,*) ' Error opening file ',
     .            inpfil(1:len_trim(inpfil)),' as old formatted'
      stop
      end
