      program fcmat
C
C       reads SIESTA FC file,
C       diagonalizes the dynamical matrix blockwise,
C       writes local coordinate system and diagonal elements
C       for each pair of neighbors,
C       for further analyzis and vizualization.
C
C       Written by A. Postnikov, apostnik@uos.de
C
      implicit none
      integer ii1,ii2,io1,io2,jj
      parameter (ii1=11,ii2=12,io1=14,io2=15)
      integer ialloc,ierr,iat,jat,nat,ix,jx,kx,ipm,iline,ityp1,ityp2,
     .         idisp,jdisp,kdisp,mdisp,nn1,nn2,in1,in2
      integer, allocatable :: ityp(:),iz(:),nna(:),
     .                        iat11(:),iat12(:),iat13(:),
     .                        iat21(:),iat22(:),iat23(:)
      character inpfil*60,outfil*60,syslab*30
      double precision cc_ang(3,3),dmin,dmax,dmax1,dmax2,sqrtwo
      double precision, allocatable :: mass(:),coor_ang(:,:),
     .                                 fc(:,:,:,:,:),
     .                  coor11(:,:),coor12(:,:),coor13(:,:),
     .                  coor21(:,:),coor22(:,:),coor23(:,:)
      double precision dc(3,3),uu(3,3),db(3,3),
     .                 dc2(6,6),uu2(6,6),db2(6,6),dmid(3),
     .                 dist,xdisp,ydisp,zdisp,dx,dy,dz,fv1(6),fv2(6),
     .                 eval(6),vec(3,3),cost1,cost2,sqrt2,
     .                 vec1(3),vec2(3),vec3(3)
C     data sqrtwo / 1.41421356237309504880 /
      character*2, allocatable :: label(:)
      external test_xv,read_xv,rs,opnout,inver3,makebox,fillbox,sortnn,
     .         locsys
C
      sqrtwo = sqrt(2.d0)
C --- read lattice vectors and atom positions from the .XV file:
      write (6,"(' Specify SystemLabel of .XV and .FC files: ')",
     .       advance="no")
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.XV'
      open (ii1,file=inpfil,form='formatted',status='old',err=801)
      call test_xv(ii1,nat)
      allocate (ityp(nat),iz(nat))
      allocate (label(nat))
      allocate (mass(nat),coor_ang(1:3,1:nat))
      allocate (fc(1:3,1:nat,1:3,1:nat,1:2),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      call read_xv(ii1,nat,ityp,iz,cc_ang,mass,label,coor_ang)
      close (ii1)

  101 write (6,*) ' Enter the limiting distance',
     .            ' to include all nearest neighbors (in Ang):'
      read (5,*) dmax1
      if (dmax1.le.0.) goto 101
  102 write (6,*) ' Enter the limiting distance',
     .            ' to include all next-nearest neighbors (in Ang):'
      read (5,*) dmax2
      if (dmax2.le.dmax1) goto 102
C
      open (io1,file='LOG',form='FORMATTED',status='unknown')
C --- call sortnn(0,... returns nn1,nn2 for alocating arrays
      call sortnn(0,nn1,nn2,nat,
     .            cc_ang,coor_ang,dmax1,dmax2,
     .            iat11,iat12,iat21,iat22,
     .            coor11,coor12,coor21,coor22)

      allocate (iat11(1:nn1),iat12(1:nn1),iat13(1:nn1))
      allocate (coor11(1:3,1:nn1),coor12(1:3,1:nn1),coor13(1:3,1:nn1))
      allocate (iat21(1:nn2),iat22(1:nn2),iat23(1:nn2))
      allocate (coor21(1:3,1:nn2),coor22(1:3,1:nn2),coor23(1:3,1:nn2),
     .          STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nn1,
     .       ' 1st neighbor pairs and ',nn2,' 2d neighbor pairs.'
        stop
      endif
C --- call sortnn(1,... find types and coordinates of atoms
C                 in pairs of 1st and 2d neighbors
      call sortnn(1,nn1,nn2,nat,
     .            cc_ang,coor_ang,dmax1,dmax2,
     .            iat11,iat12,iat21,iat22,
     .            coor11,coor12,coor21,coor22)

      call find3(nn1,nn2,iat11,iat12,iat13,iat21,iat22,iat23,
     .           coor11,coor12,coor13,coor21,coor22,coor23)
C
C --- read force constants from the .FC file:
C     write (6,"(' Specify SystemLabel of .FC file: ')",advance="no")
C     read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.FC'
      open (ii2,file=inpfil,form='formatted',status='old',err=801)
      iline = 0
      read (ii2,*)    
      do iat=1,nat   !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat   !   atom at which force is induced
          iline=iline + 1
          read (ii2,'(3f15.7)',err=301,end=302) 
     .          (fc(jx,jat,ix,iat,ipm),jx=1,3)
        enddo
      enddo
      enddo
      enddo
      close (ii2)
C
      write (6,"(' Two atom types for extracting force constants: ')",
     .       advance="no")
      read (5,*) ityp1,ityp2
   11 continue
      write (6,"(' Max and min distance (in Ang) between them: ')",
     .       advance="no")
      read (5,*) dmin,dmax
      if (dmin.lt.0..or.dmax.lt.0.) then
        write (6,*) '  dmax and dmin must not be negative'
        goto 11
      endif
      if (dmin.ge.dmax) then
        write (6,*) ' dmax must be larger than dmin'
        goto 11
      endif
C
C --- try all pairs of atoms of types ityp1 and ityp2
C ..  open file for full FC matrices, eigenvalues and eigenvectors:
      outfil = syslab(1:len_trim(syslab))//'.FCmat'
      call opnout(io1,outfil)
      write (io1,201) ityp1,ityp2,dmin,dmax
C ..  open file for the table of diagonal elements vs. bond length:
      outfil = syslab(1:len_trim(syslab))//'.FCdia'
      call opnout(io2,outfil)
      write (io2,202) ityp1,ityp2,dmin,dmax
      
C --- try if ityp1,ityp2 are nearest neighbours:
      do in1 = 1,nn1
        if  (((ityp(iat11(in1)).eq.ityp1)  .and.
     .        (ityp(iat12(in1)).eq.ityp2)) .or.
     .       ((ityp(iat11(in1)).eq.ityp2)  .and.
     .        (ityp(iat12(in1)).eq.ityp1))) then
          dist = sqrt( (coor11(1,in1)-coor12(1,in1))**2 +
     .                 (coor11(2,in1)-coor12(2,in1))**2 +
     .                 (coor11(3,in1)-coor12(3,in1))**2 )
          if (dist.le.dmax.and.dist.ge.dmin) then
C ---         analyze symmetrized force constants
            write (io2,205) 
  205  format('  Are nearest neighbours')
            dc2(:,:) = 0.d0
            do ix=1,3
            do jx=1,3
              dc2(ix,3+jx) = ( fc(jx,jat,ix,iat,1) +
     +                         fc(jx,jat,ix,iat,2) +    
     +                         fc(ix,iat,jx,jat,1) +
     +                         fc(ix,iat,jx,jat,2) )/4  
              dc2(3+ix,jx) = ( fc(jx,iat,ix,jat,1) +
     +                         fc(jx,iat,ix,jat,2) +    
     +                         fc(ix,jat,jx,iat,1) +
     +                         fc(ix,jat,jx,iat,2) )/4
            enddo
            enddo
C --- skip finding the local system, for nearest neighbours
C           call locsys(coor11(:,in1),coor12(:,in1),coor13(:,in1),
C    .                  vec1,vec2,vec3)

          endif
        endif
      enddo
C --- try if ityp1,ityp2 are next nearest neighbours:
      do in2 = 1,nn2
        if  (((ityp(iat21(in2)).eq.ityp1)  .and.
     .        (ityp(iat22(in2)).eq.ityp2)) .or.
     .       ((ityp(iat21(in2)).eq.ityp2)  .and.
     .        (ityp(iat22(in2)).eq.ityp1))) then
          dist = sqrt( (coor21(1,in2)-coor22(1,in2))**2 +
     .                 (coor21(2,in2)-coor22(2,in2))**2 +
     .                 (coor21(3,in2)-coor22(3,in2))**2 )
          if (dist.le.dmax.and.dist.ge.dmin) then
C ---         analyze symmetrized force constants
            write (io2,206) 
  206  format('  Are next nearest neighbours')
            dc2(:,:) = 0.d0
            do ix=1,3
            do jx=1,3
              dc2(ix,3+jx) = ( fc(jx,jat,ix,iat,1) +
     +                         fc(jx,jat,ix,iat,2) +    
     +                         fc(ix,iat,jx,jat,1) +
     +                         fc(ix,iat,jx,jat,2) )/4  
              dc2(3+ix,jx) = ( fc(jx,iat,ix,jat,1) +
     +                         fc(jx,iat,ix,jat,2) +    
     +                         fc(ix,jat,jx,iat,1) +
     +                         fc(ix,jat,jx,iat,2) )/4
            enddo
            enddo
C --- recover the local system, as in heuristics for fcfit.f.
C     The eigenvectors of the force constants matrix will be
C     analyzed, for easy further fitting, in relation to this
C     local system.
            call locsys(coor21(:,in2),coor22(:,in2),coor23(:,in2),
     .                  vec1,vec2,vec3)
          endif
        endif
      enddo
      
       
      mdisp = 2
      do iat=1,nat
        if (ityp(iat).eq.ityp1) then
          do jat=1,nat
C         do jat=iat,nat    !   keep jat >= iat
            if (ityp(jat).eq.ityp2) then
C gets here if iat is of type 'ityp1', jat of type 'ityp2'
C translate unit cell up to +/- mdisp along each vector:
              do idisp=-mdisp,mdisp
              do jdisp=-mdisp,mdisp
              do kdisp=-mdisp,mdisp
                xdisp = cc_ang(1,1)*idisp + 
     +                  cc_ang(1,2)*jdisp + 
     +                  cc_ang(1,3)*kdisp
                ydisp = cc_ang(2,1)*idisp + 
     +                  cc_ang(2,2)*jdisp + 
     +                  cc_ang(2,3)*kdisp
                zdisp = cc_ang(3,1)*idisp + 
     +                  cc_ang(3,2)*jdisp + 
     +                  cc_ang(3,3)*kdisp
                dx = coor_ang(1,iat)-coor_ang(1,jat) + xdisp
                dy = coor_ang(2,iat)-coor_ang(2,jat) + ydisp
                dz = coor_ang(3,iat)-coor_ang(3,jat) + zdisp
                dist = sqrt(dx**2 + dy**2 + dz**2)
                if (dist.le.dmax.and.dist.ge.dmin) then
C ---             analyze symmetrized force constants
                  if (iat.eq.jat) then  !  on-site 3*3 matrix
                    dc2(:,:) = 0.d0
                    do ix=1,3
                    do jx=1,3
                      dc2(ix,3+jx) = ( fc(jx,jat,ix,iat,1) +
     +                                 fc(jx,jat,ix,iat,2) +    
     +                                 fc(ix,iat,jx,jat,1) +
     +                                 fc(ix,iat,jx,jat,2) )/4  
                      dc2(3+ix,jx) = ( fc(jx,iat,ix,jat,1) +
     +                                 fc(jx,iat,ix,jat,2) +    
     +                                 fc(ix,jat,jx,iat,1) +
     +                                 fc(ix,jat,jx,iat,2) )/4
                    enddo
                    enddo
                    write (io1,"(/,' --- atoms ',i4,'  and ',i4,
     .                    ' at ',f12.6,'  Angstroem')") iat,jat,dist
                    write (io1,'("  Matrix of force constants:")')
                    do ix=1,3
                      write(io1,'(1p3e12.4)') (dc(ix,jx),jx=1,3)
                    enddo
C ... to diagonalize a symmetric matrix, call the RS sequence of EISPACK:
                    call rs(3,3,dc,eval,1,uu,fv1,fv2,ierr)
                    if (ierr.ne.0) then
                      print *,' ierr=',ierr,' from RS'
                      stop
                    endif
                    write (io2,204) iat,jat,dist,(eval(ix),ix=1,3)
                    write (io1,'("  Eigenvalues: ",3f10.6)')
     .                    (eval(ix),ix=1,3)
                    write (io1,'("  Eigenvectors:")') 
                    do jx=1,3
                      write(io1,'(i3,":",3f12.8)') 
     .                      jx,(uu(ix,jx),ix=1,3)
                    enddo
C --- Check the indexation of eiganvalues (row/column) as they come out of rs.
C     This was as below, and not other way around, the initial matrix
C     is recovered:
C                   do ix=1,3
C                   do jx=1,3
C                     db(ix,jx)=0.d0
C                     do kx=1,3
C                       db(ix,jx) = 
C    .                  db(ix,jx)+uu(ix,kx)*eval(kx)*uu(jx,kx)
C                     enddo
C                   enddo
C                   enddo
C                   write (io1,'("  Check multiplication:")') 
C                   do ix=1,3
C                     write(io1,'(1p3e12.4)') (dc(ix,jx),jx=1,3)
C                   enddo
C ---
                  else   !  --- iat.ne.jat:  make off-site 6*6 matrix
                    dc2(:,:) = 0.d0
                    do ix=1,3
                    do jx=1,3
                      dc2(ix,3+jx) = ( fc(jx,jat,ix,iat,1) +
     +                                 fc(jx,jat,ix,iat,2) +    
     +                                 fc(ix,iat,jx,jat,1) +
     +                                 fc(ix,iat,jx,jat,2) )/4  
                      dc2(3+ix,jx) = ( fc(jx,iat,ix,jat,1) +
     +                                 fc(jx,iat,ix,jat,2) +    
     +                                 fc(ix,jat,jx,iat,1) +
     +                                 fc(ix,jat,jx,iat,2) )/4
                    enddo
                    enddo
                    write (io1,"(/,' --- from atom ',i4,' at ( ',3f11.5,
     .                    ' )')") iat, coor_ang(1,iat) + xdisp,
     .                                 coor_ang(2,iat) + ydisp,
     .                                 coor_ang(3,iat) + zdisp
                    write (io1,"(7x,'to atom ',i4,' at ( ',3f11.5,
     .                    ' ), dist =',f11.5)") 
     .              jat,(coor_ang(ix,jat),ix=1,3),dist
                    write (io1,'("  Matrix of force constants:")')
                    do ix=1,6
                      write(io1,'(1p6e12.4)') (dc2(ix,jx),jx=1,6)
                    enddo
C ... to diagonalize a symmetric matrix, call the RS sequence of EISPACK:
                    call rs(6,6,dc2,eval,1,uu2,fv1,fv2,ierr)
                    if (ierr.ne.0) then
                      print *,' ierr=',ierr,' from RS'
                      stop
                    endif
C ... evaluate angles the major eigenvector (#6) builds
C     with the atom-to-atom vector. Note that each half of an
C     eigenvector (elements 1..3 as well as 4..6) is normalized to 1/sqrt(2) 
                    cost1 = abs(dx*uu2(1,6) 
     .                        + dy*uu2(2,6)
     .                        + dz*uu2(3,6))*sqrtwo/dist
                    cost2 = abs(dx*uu2(4,6) 
     .                        + dy*uu2(5,6)
     .                        + dz*uu2(6,6))*sqrtwo/dist
                    write (io2,204) iat,jat,dist,(eval(ix),ix=4,6),
     .                              cost1,cost2
                    write (io1,'("  Eigenvalues: ",6f10.6)')
     .                    (eval(ix),ix=1,6)
                    write (io1,'("  Eigenvectors:")') 
                    do jx=1,6
                      write(io1,'(i3,":",6f12.8)') 
     .                      jx,(uu2(ix,jx),ix=1,6)
                    enddo
C --- Check the indexation of eiganvalues (row/column) as they come out of rs.
C     This was as below, and not other way around, the initial matrix
C     is recovered:
C                   do ix=1,6
C                   do jx=1,6
C                     db2(ix,jx)=0.d0
C                     do kx=1,6
C                       db2(ix,jx) =
C    .                  db2(ix,jx)+uu2(ix,kx)*eval(kx)*uu2(jx,kx)
C                     enddo
C                   enddo
C                   enddo
C                   write (io1,'("  Check multiplication:")') 
C                   do ix=1,6
C                     write(io1,'(1p6e12.4)') (dc2(ix,jx),jx=1,6)
C                   enddo
C ---
                  endif   
                endif   ! (dist.le.dmax.and.dist.ge.dmin) 
              enddo
              enddo
              enddo
            endif
          enddo  !   jat = 1,nat
        endif
      enddo    !   iat = 1,nat
      close (io1)
      close (io2)
      stop

      stop
  301 continue
      print *,' Error reading FC file, line ',iline
      stop
  302 continue
      print *,' Unexpected end of FC file, iat,ix,ipm,jat=',
     .         iat,ix,ipm,jat     
      stop
  201 format('#',/,
     .       '#  Matrix of force constants between types ',
     .        i3,'  and ',i3,/
     .       '#  distance from ',f9.4,' to ',f9.4'  Angstroem:',/
     .       '# ')
  202 format('#',/,
     .       '#  Principal elements of force constants between types ',
     .        i3,'  and ',i3,/
     .       '#  distance from ',f9.4,' to ',f9.4' Angstroem:',/
     .       '#  iat jat   dist',9x,15('-'),' FC diag. ',15('-'),
     .        3x,'- cosines eignv-bond -')
  203 format(1x,2i4,f9.4,3x,1p9e11.3)
  204 format(1x,2i4,f9.4,3x,3f15.8,1x,2f14.8)

  801 write (6,*) ' Error opening file ',
     .            inpfil(1:len_trim(inpfil)),' as old formatted'
      stop
      end
