      subroutine expand(nat,coor0,mtau,nmult,coor1,norig)
C
C       expands coordinates of the prototype cell 
C       into coordinates over supercell with given spanning vectors
C       Input
C         nat - number of atoms in prototype cell
C         coor0(1..3,1..nat)  - fractional coordinates of atom 
C                               in the prototype vell
C         mtau(1..3,N)        - Nth spanning vector, N=1,2,3
C         nmult               - number of expanded cells
C                               [ = det(mtau), to be calculated in advance ]
C       Output
C         coor1(1..3,1..nmult*nat) - fractional coordinates
C                                    (in lattice vectors of prototype cell),
C                                    expanded over supercell
C         norig(1..mult)      - index of prototype atom 
C                               expanded into given supercell atom 
      implicit none
      integer nat,nmult,mtau(3,3),norig(nat*nmult)
      double precision coor0(3,nat),coor1(3,nat*nmult),
     .                 rmtau(3,3),rminv(3,3)
      integer nmult1,ii,jj,kk,ll,m_min(3),m_max(3),nadd,iat
      double precision coor_t(3),coor_m(3)
      external inver3
C
C --- for the case, check that nmult is correct:
      nmult1 = mtau(1,1)*mtau(2,2)*mtau(3,3) +
     +         mtau(1,2)*mtau(2,3)*mtau(3,1) +
     +         mtau(1,3)*mtau(2,1)*mtau(3,2) -
     -         mtau(1,1)*mtau(2,3)*mtau(3,2) -
     -         mtau(1,2)*mtau(2,1)*mtau(3,3) -
     -         mtau(1,3)*mtau(2,2)*mtau(3,1)
      if (abs(nmult1).ne.nmult) then
        print *, 'Stop in subr. expand: wrong nmult value'
        stop
      endif
      rmtau(:,:) = mtau(:,:)
      call inver3(rmtau,rminv)
C     print *, ' --- rmtau --- '
C     print ('(3f12.6)'), (rmtau(1,ii),ii=1,3)
C     print ('(3f12.6)'), (rmtau(2,ii),ii=1,3)
C     print ('(3f12.6)'), (rmtau(3,ii),ii=1,3)
C     print *, ' --- rminv --- '
C     print ('(3f12.6)'), (rminv(1,ii),ii=1,3)
C     print ('(3f12.6)'), (rminv(2,ii),ii=1,3)
C     print ('(3f12.6)'), (rminv(3,ii),ii=1,3)
C --- construct a "cube" which fully includes supercell:
      m_min(:) = min( 0, mtau(:,1), mtau(:,2), mtau(:,3),
     .           mtau(:,1)+mtau(:,2), mtau(:,1)+mtau(:,3),
     .           mtau(:,2)+mtau(:,3), mtau(:,1)+mtau(:,2)+mtau(:,3) )
      m_max(:) = max( 0, mtau(:,1), mtau(:,2), mtau(:,3),
     .           mtau(:,1)+mtau(:,2), mtau(:,1)+mtau(:,3),
     .           mtau(:,2)+mtau(:,3), mtau(:,1)+mtau(:,2)+mtau(:,3) )
      nadd = 0
      do kk = m_min(3),m_max(3)
      do jj = m_min(2),m_max(2)
      do ii = m_min(1),m_max(1)
        loop_atoms: do iat = 1,nat
C ---     try atom iat, expanded into (ii,jj,kk) :
C         write (6,"(' ii,jj,kk=',3i4,' iat=',i3 )") ii,jj,kk,iat
C         print *, ' coor0 =', coor0(1,iat),coor0(2,iat),coor0(3,iat)
          coor_t(1) = real(ii) + coor0(1,iat)
          coor_t(2) = real(jj) + coor0(2,iat)
          coor_t(3) = real(kk) + coor0(3,iat)
C ---     whether it really fits into the supercell
          do ll = 1,3
            coor_m(ll) = rminv(ll,1)*coor_t(1) +
     +                   rminv(ll,2)*coor_t(2) +
     +                   rminv(ll,3)*coor_t(3) 
C         write (6,"(' coor_t =',3f10.5,'  coor_m =',3f10.5)") 
C    .           coor_t,coor_m
C           if ( coor_m(ll).lt.0.0   .or. 
C    .           coor_m(ll).ge.1.0 ) cycle loop_atoms
            if ( coor_m(ll).lt.-0.000001   .or. 
     .           coor_m(ll).gt. 0.999999 ) cycle loop_atoms
          enddo
C ---     all coor_m are 0<= coor_m < 1; this atom projects
C         into the supercell; add it to the list:
          nadd = nadd+1
C         write (6,"(' Added to nadd=',i4,' ii,jj,kk,iat=',4i4)")
C    .           nadd,ii,jj,kk,iat
C         write (6,"(' coor_t =',3f10.5,'  coor_m =',3f10.5)") 
C    .           coor_t,coor_m
          norig(nadd) = iat
          coor1(:,nadd) = coor_t(:)
        enddo loop_atoms
      enddo
      enddo
      enddo
C --- check the number of atoms found:
      if (nadd.ne.nmult*nat) then
        print *, 'Error in subr. expand: nadd=',nadd,
     .           ' differs from expected nmult*nat=',nmult*nat
        stop
      endif
      return
      end
