      program fcexp
C
C       FC expander for a perfect-crystal supercell: 
C       reads SIESTA XV file and (partial) FC file,
C       and updates the latter towards the correct FC file of supercell.
C
C       Written by A. Postnikov, andrei.postnikov@univ-lorraine.fr
C
      implicit none
      integer ii1,ii2,ii3,io1
      parameter (ii1=11,ii2=12,ii3=13,io1=14)
      character inpfil*60, xvname*60, fc1nam*60, fc2nam*60
      integer ialloc,ierr,iat,jat,iat1,jat1,nat1,nat2,ix,jx,ipm,iline
      integer, allocatable :: ityp(:),iz(:),nat0(:),mper(:,:),icod(:)
      logical, allocatable :: sele(:)
      double precision cc_ang(3,3)
      double precision, allocatable :: mass(:),coor_ang(:,:),
     .                                 fc(:,:,:,:,:)
      character*2, allocatable :: label(:)
      external test_xv,read_xv,mkperm
C
C
      write (6,201,advance="no")
      read (5,*) inpfil
      write (6,"(' ')")
      open (ii1,file=inpfil(1:len_trim(inpfil)),form='formatted',
     .      status='old',err=801)
      read (ii1,*) xvname
      open (ii2,file=xvname(1:len_trim(xvname)),form='formatted',
     .      status='old',err=802)
C --- read lattice vectors and atom positions from the .XV file:
      call test_xv(ii2,nat2)
      allocate (ityp(nat2),iz(nat2),icod(nat2))
      allocate (label(nat2))
      allocate (sele(nat2))
      allocate (mass(nat2),coor_ang(1:3,1:nat2),mper(1:nat2,1:nat2),
     .          STAT=ialloc)
C ---  labels and masses wont't be used in fact...
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat2,' atoms.'
        stop
      endif
      call read_xv(ii2,nat2,ityp,iz,cc_ang,mass,label,coor_ang)
      close (ii2)
      read (ii1,*,err=803) nat1
      allocate (nat0(1:nat1),STAT=ialloc)
      allocate (fc(1:3,1:nat2,1:3,1:nat1,1:2),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat1,' atoms.'
        stop
      endif
      read (ii1,*,err=804) (nat0(iat),iat=1,nat1)
C --- check consistency of "selected" list:
      if (nat1.gt.nat2) then
        write (6,*) ' ERROR: nat1=',nat1,' from ',
     .              inpfil(1:len_trim(inpfil)),' exceeds nat2=',nat2,
     .              ' from ',xvname(1:len_trim(xvname))
        stop
      endif
      icod(:)=0   !  use icod() temporarily for bookkeeping
      do iat = 1,nat1
        if(nat0(iat).gt.nat2) then
          write (6,*) ' ERROR: nat0(',iat,')=',nat0(iat),' from ',
     .              inpfil(1:len_trim(inpfil)),' exceeds nat2=',nat2,
     .              ' from ',xvname(1:len_trim(xvname))
          stop
        elseif(icod(nat0(iat)).eq.-1) then
          write (6,*) ' ERROR: nat0(',iat,')=',nat0(iat),
     .                ' repeatedly appears in the 3d line of ',
     .                inpfil(1:len_trim(inpfil))
          stop
        else
          icod(nat0(iat)) = -1
        endif
      enddo
C     Test passed. icod(:) will be now redifined in subr. mkperm.
C --- mark "selected" atoms in the whole list,
C     for convenience of further use:
      sele(:) = .false.
      do iat = 1,nat1
        sele(nat0(iat)) = .true.
      enddo
C --- Structure read in; now we construct the permutation matrix,
C     with which the force constants will be straightforwardly read in
C     and wrote down updated and in the correct order.
C     The meaning of it, mper(iat1,iat2), is the following.
C     If iat1 is within the "inequivalent" ("selected") ones,
C     iat2 simply runs through all atoms of the supercell, 1 .. nat2.
C     If iat1 is NOT a "selected" one, the diagonal element mper(iat1,iat1) 
C     gives the "selected" atom to which iat1 is equivalent. 
C     Otherwise, mper(iat1,iat2) points to the atom in the supercell
C     which structurally stands (with all its replicas, taking translations 
C     of the supercell into account) exactly so to mper(iat1,iat1) 
C     as iat2 stands to iat1.
C     This permits, for updating the force constants, replace the
C     (unknown) block [iat2,iat1] 
C     (iat1: atom displaced; iat2: atom on which the force is probed)
C     by the (known) one [mper(iat1,iat2),mper(iat1,iat1)].
C     This is like a "parallel translation" of force constant blocks
C     through supercell boundaries, reshuffling the atoms numeration 
C     but preserving Cartesian coordinates.
C     In case of doubt, the mper() matrix can be prepared by hand
C     and provided in the input, 
C     but hopefully the following script will manage to construct it
C     automatically.
C     
      call mkperm(nat1,nat2,ityp,icod,nat0,sele,cc_ang,coor_ang,mper)

      read (ii1,*) fc1nam
      open (ii3,file=fc1nam(1:len_trim(fc1nam)),form='formatted',
     .      status='old',err=805)
      iline = 0
      read (ii3,*)   !   Header line; ignored    
      do iat=1,nat1  !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat2  !   atom at which force is induced
          iline=iline + 1
          read (ii3,'(3f15.7)',err=806,end=807) 
     .         (fc(jx,jat,ix,iat,ipm),jx=1,3)
        enddo
      enddo
      enddo
      enddo
      close (ii3)
C --- Prepare output file...
      read (ii1,*) fc2nam
      open (io1,file=fc2nam(1:len_trim(fc2nam)),form='formatted',
     .      status='new',err=808)
      write (io1,301)   !   Header line
C --- Write down force constants, in permuted block order
      do iat=1,nat2  !   atom which is displaced
         iat1 = mper(iat,iat)
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat2  !   atom at which force is induced
          jat1 = mper(iat,jat)
          write (io1,302) (fc(jx,jat1,ix,iat1,ipm),jx=1,3)
        enddo
      enddo
      enddo
      enddo
      close (io1)
      write (6,*) ' New FC file created; bye'

      stop

  201 format (' FCexp expects an input file',/
     .        ' which contains, free format and line by line,',
     .        ' the following information:',/
     .        ' - 1st line: name of supercell XV file;',/
     .        ' - 2d  line: number of inequivalent atoms in it',/
     .        '       (i.e. those who make a - not necessarily',
     .        ' compact - primitive cell);',/
     .        " - 3d  line: list of these atoms' numbers in XV;",/
     .        ' - 4th line: name of "partial" FC file.',
     .        ' It must be created',/
     .        ' displacing inequivalent atoms within the supercell,',/
     .        ' in the sequence as given above, while the forces are',
     .        ' induced',/' over the whole supercell.',
     .        ' This file must contain (N_ineq*N_scell*6)+1 lines;',/
     .        ' - 5th line: name of the resulting "full" FC file.',/
     .        ' Please pass the name of this 5-line file: ')
  301 format ('Force constants matrix')
  302 format (3f15.7)

  801 write (6,*) ' Error opening input file ',
     .            inpfil(1:len_trim(inpfil)),' as old formatted'
      stop
  802 write (6,*) ' Error opening XV file ',
     .            xvname(1:len_trim(xvname)),' as old formatted'
      stop
  803 write (6,*) ' Error reading number of atoms',
     .            ' from the 2d line of ',inpfil(1:len_trim(inpfil))
      stop
  804 write (6,*) ' Error reading list of inequivalent atoms',
     .            ' from the 3d line of ',inpfil(1:len_trim(inpfil)),
     .            ' :'
      write (6,*) ' expecting ',nat1,' entries!'
      stop
  805 write (6,*) ' Error opening FC file ',
     .            fc1nam(1:len_trim(fc1nam)),' as old formatted'
      stop
  806 continue
      print *,' Error reading FC file ',fc1nam(1:len_trim(fc1nam)),
     .        ' line ',iline
      stop
  807 continue
      print *,' Unexpected end of FC file ',fc1nam(1:len_trim(fc1nam)),
     .        ': iat,ix,ipm,jat=',iat,ix,ipm,jat     
      stop
  808 write (6,*) ' Error opening file ',
     .            fc2nam(1:len_trim(fc2nam)),' as new'
      stop
      end
