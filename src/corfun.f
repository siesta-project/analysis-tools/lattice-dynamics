C...............................................................
C
      subroutine corfun(itrun,itcor,afun,acfun,anorm)
C
C     calculates autocorrelation function
C     following an example from Chapter 6 of the book 
C     by M.P.Allen and D.J.Tildesley
C     "Computer Simulation of Liquids", Clarendon, Oxford, 1987
C     ( ISBN 0-19-855645-4 )
C
C  Input
C     afun(1:itrun) - time-dependent function
C     itrun         : number of steps in input function
C     itcor         : number of steps in correlation function
C  Output
C     acfun(0:itcor)  : autocorrelation function      
C     anorm
C
      implicit none 
      integer itrun,itcor
      double precision afun(itrun),acfun(0:itcor),anorm(0:itcor)
      integer it,it0,itt0,itt0mx
      double precision a0

      acfun = 0.0
      anorm = 0.0
      do it0 = 1,itrun
        a0 = afun(it0)
        itt0mx = min(itrun,it0+itcor)
        do itt0 = it0, itt0mx
          it = itt0 - it0
          acfun(it) = acfun(it) + a0*afun(itt0)
          anorm(it) = anorm(it) + 1.0
        enddo
      enddo
      acfun = acfun/anorm
      return
      end
