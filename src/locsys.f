C...............................................................
C
      subroutine locsys(coor1,coor2,coor3,vec1,vec2,vec3)
C
C     constructs local coordinate system based on coordinates
C     of three atoms:
C      vec1 from (coor1) to (coor2);
C      vec2 is normal to the (coor1-coor2-coor3) plane;
C      vec3 is in the (coor1-coor2-coor3) plane, normal to vec1.
C
C     Input:
C       coor1        : coordinates of 1st atom
C       coor2        : coordinates of 2d  atom
C       coor3        : coordinates of 3d (added) atom
C     Output:
C        vec1        : 1st vector of the local system
C        vec2        : 2d  vector of the local system
C        vec3        : 3d  vector of the local system
C        (the vectors are orthonormal)
C
      implicit none
      double precision coor1(3),coor2(3),coor3(3),
     .                 vec1(3),vec2(3),vec3(3),vcnorm
      
C     
C     vec1: main axis, connecting atoms 1 and 2
       vec1 = coor2 - coor1
       vcnorm = sqrt( vec1(1)**2 + vec1(2)**2 + vec1(3)**2 )
       vec1 = vec1/vcnorm 
C
C     vec2: normal to the plane of three atoms
C             |        i                j                 k         |
C      vec2 = |  [coor2-coor1]_x  [coor2-coor1]_y  [coor2-coor1]_z  |
C             |  [coor3-coor1]_x  [coor3-coor1]_y  [coor3-coor1]_z  |
C
      vec2(1) = ( coor2(2)-coor1(2) )*( coor3(3)-coor1(3) ) -
     -          ( coor2(3)-coor1(3) )*( coor3(2)-coor1(2) )
      vec2(2) = ( coor2(3)-coor1(3) )*( coor3(1)-coor1(1) ) -
     -          ( coor2(1)-coor1(1) )*( coor3(3)-coor1(3) )
      vec2(3) = ( coor2(1)-coor1(1) )*( coor3(2)-coor1(2) ) -
     -          ( coor2(2)-coor1(2) )*( coor3(1)-coor1(1) )
       vcnorm = sqrt( vec2(1)**2 + vec2(2)**2 + vec2(3)**2 )
       vec2 = vec2/vcnorm 
C
C     vec3: in the plane of three atoms; normal to vec1 and vec2
C
      vec3(1) = vec1(2)*vec2(3) - vec1(3)*vec2(2)
      vec3(2) = vec1(3)*vec2(1) - vec1(1)*vec2(3)
      vec3(3) = vec1(1)*vec2(2) - vec1(2)*vec2(1)
       vcnorm = sqrt( vec3(1)**2 + vec3(2)**2 + vec3(3)**2 ) 
       vec3 = vec3/vcnorm 

      return
      end
