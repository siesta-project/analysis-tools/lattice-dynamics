C
C   phdos, a script to calculte phonon density of states
C          in a supercell,
C          using phonon eigenvectors provided by "vibrator"

C          Written by Andrei Postnikov, Oct 2005   Vers_0.2
C            XV read format bug fixed   May 2006
C     "polarization" projection added   March 2012
C         "symmetry" projection added   July 2012
C     
C          andrei.postnikov@univ-lorraine.fr
C
      program phdos
      implicit none
      integer ii1,ii2,ii3,io1,io2,io3,io4,ivmin,ivmax
      parameter (ii1=11,ii2=12,ii3=13,io1=21,io2=22,io3=23,io4=24)
      integer ialloc,iat,nat,iatmin,iatmax,mode,nodif,idif,npoints
      integer, allocatable :: ityp(:),iz(:),jat(:,:),jtyp(:),nna(:)
      double precision tau(3,3),qvec(3),qq(3),delta,pvec(3),pmod
      double precision, allocatable :: mass(:),coor(:,:),cosym(:,:),
     .       freq(:),evr(:,:,:),evi(:,:,:),wwsum(:,:),zz(:),
     .       wwpro1(:,:),wwpro2(:,:),wwpro3(:,:)
      character inpfil*60,outfil*60,symfil*60,syslab*30
     .          ,modlab*1,plab*1
      character*2, allocatable :: label(:)
      external test_xv,read_xv,read_ev,read_sym,
     .         full_peaks,full_smear,qres_peaks,qres_smear,
     .         symp_peaks,symp_smear
C
C     string manipulation functions in Fortran used below:
C     len_trim(string): returns the length of string
C                       without trailing blank characters,

C --- read lattice vectors and atom positions from the .XV file:
      write (6,701)
  701 format(' Specify SystemLabel of .XV file: ',$)
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.XV'
      open (ii1,file=inpfil,form='formatted',status='old',err=801)
      call test_xv(ii1,nat)
      allocate (ityp(nat))
      allocate (iz(nat))
      allocate (mass(nat))
      allocate (label(nat))
      allocate (jtyp(nat))
      allocate (nna(nat))
      allocate (jat(nat,nat))
      allocate (cosym(1:3,1:nat))  ! -- neede for symmetry proj.only (mode 4)
      allocate (coor(1:3,1:nat),STAT=ialloc) 
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      call read_xv(ii1,nat,ityp,iz,tau,mass,label,coor)
      close (ii1)                                                     
C --- count different types in the XV file. Note that they can be changed
C     arbitrarily (by hand) in the XV file  as compared to the Siesta run. 
C     E.g. to select some atoms of type=4 as different, assign them type=14.
C     They will then get a separate column in output files.
      nodif=0
      do 98 iat=1,nat
        if (nodif.gt.0) then
          do idif=1,nodif
            if (ityp(iat).eq.jtyp(idif)) then  !  add atom to existing type
               nna(idif)=nna(idif)+1
               jat(idif,nna(idif))=iat
               goto 98
            endif
          enddo
        endif
        nodif=nodif+1 !  add a new type
        jtyp(nodif)=ityp(iat)
        nna(nodif)=1
        jat(nodif,1)=iat
   98 continue
      write (6,"(i5,' different types found in the XV file:')") nodif
      do idif=1,nodif
        write (6,"(i5,' atoms of type ',i5)") nna(idif),jtyp(idif)
      enddo
C --- read and store frequencies /eigenvectors from the .vector file,
C     q =(0 0 0)  only :
C     allocate for all modes, 1 through nat*3 :
      ivmin=1
      ivmax=nat*3
C     (change ivmin, ivmax above if want to restrict to less modes)
      allocate (freq(ivmin:ivmax))
      allocate (evr(1:3,1:nat,ivmin:ivmax))
      allocate (evi(1:3,1:nat,ivmin:ivmax))
      allocate (zz(1:nodif))
      allocate (wwsum(1:nodif,ivmin:ivmax))
      allocate (wwpro1(1:nodif,ivmin:ivmax))
      allocate (wwpro2(1:nodif,ivmin:ivmax))
      allocate (wwpro3(1:nodif,ivmin:ivmax),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for vibration modes'
        stop
      endif
      write (6,702)
  702 format(' Specify SystemLabel of .vectors file: ',$)
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.vectors'
      open (ii2,file=inpfil,form='formatted',status='old',err=801)
      call read_ev(ii2,nat,qq,ivmin,ivmax,evr,evi,freq)
      write (6,*) 'opened and read ',inpfil
      close (ii2)
C ---------------------------------------------------
      write (6,*) 'You have three options of constructing mode density:'
      write (6,*) '- Total density of modes ',
     .            '(sum of squares of eigenvectors),'
      write (6,*) '- Q-projected density of modes ',
     .            '(convolution with a given Q-vector), or'
      write (6,*) '- Symmetry-projected density of states ',
     .            '(using an auxiliary symmetry file).'

  101 write (6,703) 
  703 format(' Do you want total density of states (T),',
     .       ' Q-vector convolution (Q),'
     .       ' or Symmetry projection (S)? : ',$)
      read (5,*,err=101,end=101) modlab
      if (modlab.eq.'T'.or.modlab.eq.'t') then
        mode=1
      else if (modlab.eq.'Q'.or.modlab.eq.'q') then
        mode=2
        pvec(:)=0.0
        write (6,*) 'Type three numbers qx qy qz for convolution. ',
     .              'The units are 1/Bohr.'
        write (6,*) 'So for the X point of simple cubic lattice with ',
     .              'A=5 Bohr type:  0.1  0  0'
  102   write (6,704)
  704   format (' Enter  qx qy qz : ',$)
        read (5,*,err=102,end=102) qvec
  103   write (6,705) 
  705 format(' Do you want sUm of sqUares (U) ',/
     .       ' or Projection onto a given Polarisation',
     .       ' direction (P) ? : ',$)
        read (5,*,err=101,end=101) plab
        if (plab.eq.'P'.or.plab.eq.'p') then
          mode=3
  104     write (6,706) 
  706 format(' Type three direction numbers for polarisation vector ',/
     .       ' (normalization irrelevant) : ',$)
          read (5,*,err=104,end=104) pvec
          pmod = sqrt(pvec(1)**2+pvec(2)**2+pvec(3)**2)
          pvec = pvec/pmod
        elseif (plab.ne.'U'.and.plab.ne.'u') then
          goto 103
        endif
      elseif (modlab.eq.'S'.or.modlab.eq.'s') then
        mode=4
C       ... specify the details of symmetry projection later on...
      else 
        goto 101   ! none of eligible labels
      endif
C ---------------------------------------------------
C     Common for all modes:
C --- output file(s) .WS? contain the same information as .VS?,
C     but smeared, as an (continuous) functions of frequency. 
C     Fix smearing parameter and No. of steps, to avoid many questions:
C     delta = 10.0
      delta =  2.0
      npoints = 2000
C 
      if (mode.eq.1) then
        outfil = syslab(1:len_trim(syslab))//'.VST'
        open (io1,file=outfil,form='formatted',status='new',err=802)
        call full_peaks(io1,nat,jat,label,qq,nodif,jtyp,nna,
     .                  ivmin,ivmax,freq,evr,evi,wwsum)       
        close (io1)
        outfil = syslab(1:len_trim(syslab))//'.WST'
        open (io2,file=outfil,form='formatted',status='new',err=802)
        call full_smear(delta,npoints,io2,ivmin,ivmax,nat,jat,qq,
     .                  nodif,jtyp,label,freq,wwsum,zz)
        close (io2)
      else if ((mode.eq.2).or.(mode.eq.3)) then
        outfil = syslab(1:len_trim(syslab))//'.VSQ'
        open (io1,file=outfil,form='formatted',status='new',err=802)
        if (mode.eq.3) then
          outfil = syslab(1:len_trim(syslab))//'.VSQ-L'
          open (io2,file=outfil,form='formatted',status='new',err=802)
          outfil = syslab(1:len_trim(syslab))//'.VSQ-T'
          open (io3,file=outfil,form='formatted',status='new',err=802)
        endif
        call qres_peaks(io1,io2,io3,mode,pvec,nat,jat,label,qq,
     .                  nodif,jtyp,nna,coor,ivmin,ivmax,freq,evr,evi,
     .                  qvec,wwpro1,wwpro2,wwpro3)       
        close (io1)
        if (mode.eq.3) then
          close (io2)
          close (io3)
        endif
        outfil = syslab(1:len_trim(syslab))//'.WSQ'
        open (io1,file=outfil,form='formatted',status='new',err=802)
        write(io1,201) qq,qvec,delta
        write(io1,204) 
        call qres_smear(delta,npoints,io1,ivmin,ivmax,nat,jat,
     .                  nodif,jtyp,label,freq,wwpro1,zz)
        close (io1)
        if (mode.eq.3) then
          outfil = syslab(1:len_trim(syslab))//'.WSQ-L'
          open (io2,file=outfil,form='formatted',status='new',err=802)
          write(io2,201) qq,qvec,delta
          write(io2,202) pvec
          write(io2,204) 
          call qres_smear(delta,npoints,io2,ivmin,ivmax,nat,jat,
     .                    nodif,jtyp,label,freq,wwpro2,zz)
          close (io2)
          outfil = syslab(1:len_trim(syslab))//'.WSQ-T'
          open (io3,file=outfil,form='formatted',status='new',err=802)
          write(io3,201) qq,qvec,delta
          write(io3,203) pvec
          write(io3,204) 
          call qres_smear(delta,npoints,io3,ivmin,ivmax,nat,jat,
     .                    nodif,jtyp,label,freq,wwpro3,zz)
          close (io3)
        endif
      else if (mode.eq.4) then
        write (6,707)
  707 format(' Name of Symmetry file for projection: ',$)
        read (5,*) symfil
        inpfil = symfil(1:len_trim(symfil))
        open (ii3,file=inpfil,form='formatted',status='old',err=801)
        call read_sym(ii3,nat,cosym)
        close (ii3)                                                     

        outfil = syslab(1:len_trim(syslab))//'_'//
     .           symfil(1:len_trim(symfil))//'.VSS'
        open (io1,file=outfil,form='formatted',status='new',err=802)

        call symp_peaks(io1,nat,cosym,jat,label,qq,nodif,symfil,
     .                  jtyp,nna,ivmin,ivmax,freq,evr,evi,wwsum)       

        close (io1)
        outfil = syslab(1:len_trim(syslab))//'_'//
     .           symfil(1:len_trim(symfil))//'.WSS'
        open (io2,file=outfil,form='formatted',status='new',err=802)

        call symp_smear(delta,npoints,io2,ivmin,ivmax,nat,jat,qq,
     .                  nodif,symfil,jtyp,label,freq,wwsum,zz)

        close (io2)
      endif
C
      deallocate (ityp,iz,mass,label,jtyp,nna,jat,coor)
      deallocate (freq,evr,wwsum,zz)
      stop

  201 format('#     Phonon eigenvectors for Q = (',3f10.6,' )',/
     .       '#          convoluted with qvec = (',3f10.6,' )',/
     .       '#          smeared with ',f8.4 )
  202 format('#  projected onto polariz. vector (',3f10.6,' )')
  203 format('#  projected onto plane normal to (',3f10.6,' )')
  204 format('#          and taken to square')

                                                           
  801 write (6,*) ' Error opening file ',
     .            inpfil(1:len_trim(inpfil)),' as old formatted'
      stop 
  802 write (6,*) ' Error opening file ',
     .            outfil(1:len_trim(inpfil)),' as new'
      stop 
      end
C
C...............................................................
C
	subroutine trans_1(MAT,MEV,nat,nev,nolab,nna,jat,idisp,
     .	                   lab,evr,evi,freq,qvec,wwr,wwi)
C       Fourier transform eigenvectors,
C       sum up over types and cartesian directions
C       and take to square
C
	implicit none
	real*8 twopi
	parameter (twopi=6.283185307)
	integer MAT,MEV,nat,i,j,iev,nev,nolab,ilab,nrat,
     .          idisp(3,MAT),nna(MAT),jat(MAT,MAT)
	real*8 evr(3,MAT,MEV),evi(3,MAT,MEV),freq(MEV),
     .         qvec(3),wwr(3,MAT),wwi(3,MAT),ww(3),arg,sinarg,cosarg        
	character*6  lab(MAT)

	do iev=1,nev
	  write (6,204) iev, freq(iev)
  204  format('    iev=',i4,'  frequency = ',f10.4)
	do ilab=1,nolab
	  do i=1,3
	    wwr(i,ilab)=0.d0
	    wwi(i,ilab)=0.d0
	  enddo
	  do j=1,nna(ilab)  !  loop over atoms within the same type
	    nrat = jat(ilab,j)
	    arg=idisp(1,nrat)*qvec(1) +
     +          idisp(2,nrat)*qvec(2) +
     +          idisp(3,nrat)*qvec(3)
	    cosarg = cos(twopi*arg)
	    sinarg = sin(twopi*arg)
	    do i=1,3
	      wwr(i,ilab) = wwr(i,ilab) +
     +        evr(i,nrat,iev)*cosarg - evi(i,nrat,iev)*sinarg
	      wwi(i,ilab) = wwi(i,ilab) +
     +        evr(i,nrat,iev)*sinarg + evi(i,nrat,iev)*cosarg
	    enddo
	  enddo
	  do i=1,3
	    ww(i) = wwr(i,ilab)*wwr(i,ilab) + wwi(i,ilab)*wwi(i,ilab)
	  enddo
 	  write (6,205) ilab, lab(ilab), ww
  205   format (i6,a6,3f12.6)
	enddo
	enddo
	return
	end
C
C...............................................................
C
      subroutine full_peaks(io1,nat,jat,label,qq,nodif,jtyp,nna,
     .	                    ivmin,ivmax,freq,evr,evi,wwsum)
C
C     Takes square of eigenvectors,
C     sums up over all atoms in type
C
      implicit none
      integer io1,iev,nodif,idif,jtyp(nat),nna(nat),
     .        nat,jat(nat,nat),nrat,ii,jj,ivmin,ivmax
      double precision freq(ivmin:ivmax),
     .                 evr(3,nat,ivmin:ivmax),evi(3,nat,ivmin:ivmax),
     .                 wwsum(nodif,ivmin:ivmax),qq(3)
      character*2 label(nat)

      write (io1,201) qq
      write (io1,202) (jtyp(idif),idif=1,nodif)
      write (io1,203) (label(jat(idif,1)),idif=1,nodif)
      write (io1,204) 
C
      do iev=ivmin,ivmax
        do idif=1,nodif
          wwsum(idif,iev)=0.d0
          do jj=1,nna(idif)  !  loop over atoms within the same type
            nrat = jat(idif,jj)
            do ii=1,3
              wwsum(idif,iev) = wwsum(idif,iev) +
     +        evr(ii,nrat,iev)**2 + evi(ii,nrat,iev)**2
            enddo
          enddo
        enddo
        write (io1,205) freq(iev),(wwsum(idif,iev),idif=1,nodif)
      enddo
      return
  201 format('#   Sum of squares of eigenvectors for Q =(',3f10.6,' )')
  202 format('#   Freq  ',2x,100(2x,i4,4x))
  203 format('#  (cm-1) ',2x,100(4x,a2,4x))
  204 format('#') 
  205 format (f10.3,100(f10.4))
      end
C
C...............................................................
C
      subroutine full_smear(delta,npoints,io2,ivmin,ivmax,nat,jat,
     .                      qq,nodif,jtyp,label,freq,wwsum,zz)
C     Smear the peaks of phonon density of states 
C     (frequencies in  "freq", squared eigenvectors in "wwsum")
C     with a width parameter delta
C
      implicit none
      integer npoints,io2,iev,ivmin,ivmax,nodif,idif,ii,
     .        jtyp(nodif),nat,jat(nat,nat)
      double precision freq(ivmin:ivmax),wwsum(nodif,ivmin:ivmax),
     .       zz(nodif),xx,delta,xmin,xmax,xstep,broad,qq(3)
      character*2 label(nat)
      external broad

C     determine limits and mesh, to avoid many questions:
      xmin = freq(ivmin) - (freq(ivmax)-freq(ivmin))*0.15
      xmax = freq(ivmax) + (freq(ivmax)-freq(ivmin))*0.15
      xstep=(xmax-xmin)/(npoints-1)

      write (io2,201) qq,delta
      write (io2,202) (jtyp(idif),idif=1,nodif)
      write (io2,203) (label(jat(idif,1)),idif=1,nodif)
      write (io2,204) 
      do ii=1,npoints
        xx = xmin+(ii-1)*xstep
        do idif=1,nodif
          zz(idif)=0.d0
          do iev=ivmin,ivmax
C ---       discard acoustic mode of zero frequency in total DOS
            if ( freq(iev).gt.1.d0  ) then
              zz(idif)=zz(idif) +
     +                 broad(xx-freq(iev),delta)*wwsum(idif,iev)
            endif 
          enddo
        enddo
        write (io2,205) xx,(zz(idif),idif=1,nodif)
      enddo
      return
  201 format('# ',/
     .       '#   Total phonon DOS for supercell Q = (',3f10.6,' )',/
     .       '#   Sum of squares of eigenvectors, smeared with ',f8.4)
  202 format('#   Freq  ',2x,100(2x,i4,4x))
  203 format('#  (cm-1) ',2x,100(4x,a2,4x))
  204 format('#') 
  205 format (f10.4,100(f10.4))
      end
C
C...............................................................
C
      subroutine qres_peaks(io1,io2,io3,mode,pvec,nat,jat,label,qq,
     .                      nodif,jtyp,nna,coor,ivmin,ivmax,freq,
     .                      evr,evi,qvec,wwpro1,wwpro2,wwpro3)
C
C     Fourier transforms eigenvectors, 
C     sums up over types and cartesian directions
C     and takes to square
C
      implicit none
      integer io1,io2,io3,mode,iev,nodif,idif,jtyp(nat),nna(nat),
     .        nat,jat(nat,nat),nrat,ii,jj,ivmin,ivmax
      double precision freq(ivmin:ivmax),
     .                 evr(3,nat,ivmin:ivmax),evi(3,nat,ivmin:ivmax),
     .                 wwpro1(nodif,ivmin:ivmax),
     .                 wwpro2(nodif,ivmin:ivmax),
     .                 wwpro3(nodif,ivmin:ivmax),
     .                 wwr(3),wwi(3),coor(3,nat),
     .                 qq(3),qvec(3),pvec(3),twopi,arg,sinarg,cosarg
      character*2 label(nat)
      parameter (twopi=6.283185307)

      write (io1,201) qq,qvec
      write (io1,206) 
      write (io1,204) (jtyp(idif),idif=1,nodif)
      write (io1,205) (label(jat(idif,1)),idif=1,nodif)
      write (io1,206) 
      if (mode.eq.3) then
        write (io2,201) qq,qvec
        write (io2,202) pvec
        write (io2,204) (jtyp(idif),idif=1,nodif)
        write (io2,205) (label(jat(idif,1)),idif=1,nodif)
        write (io2,206) 
        write (io3,201) qq,qvec
        write (io3,203) pvec
        write (io3,204) (jtyp(idif),idif=1,nodif)
        write (io3,205) (label(jat(idif,1)),idif=1,nodif)
        write (io3,206) 
      endif
C
      wwpro1(:,:)=0.d0
      wwpro2(:,:)=0.d0
      wwpro3(:,:)=0.d0
      do iev=ivmin,ivmax
        do idif=1,nodif
          wwr(:)=0.d0
          wwi(:)=0.d0
          do jj=1,nna(idif)  !  loop over atoms within the same type
            nrat = jat(idif,jj)
            arg=coor(1,nrat)*qvec(1) +
     +          coor(2,nrat)*qvec(2) +
     +          coor(3,nrat)*qvec(3)
            cosarg = cos(twopi*arg)
            sinarg = sin(twopi*arg)
            do ii=1,3
              wwr(ii) = wwr(ii) +
     +        evr(ii,nrat,iev)*cosarg - evi(ii,nrat,iev)*sinarg
              wwi(ii) = wwi(ii) +
     +        evr(ii,nrat,iev)*sinarg + evi(ii,nrat,iev)*cosarg
            enddo
          enddo      !  jj=1,nna(idif)
C         [ wwr + i*wwi ] is now (complex) q-projection vector
C         over all atoms of the same type, see Eq.(1) of PRB82-205204 ;
C         'pvec' is the (normalized to 1.0) polarization vector
C ---     Accumulate q-projection vector squared :
C              | [wwr + i*wwi] |^2
          wwpro1(idif,iev) = wwpro1(idif,iev) +
     +           wwr(1)**2 + wwr(2)**2 + wwr(3)**2 +
     +           wwi(1)**2 + wwi(2)**2 + wwi(3)**2 
          if (mode.eq.3) then
C ---     Accumulate q-projection vector projected onto 'pvec', then squared :
C              | [wwr + i*wwi] \cdot pvec |^2
          wwpro2(idif,iev) = wwpro2(idif,iev) + 
     +          (wwr(1)*pvec(1) + wwr(2)*pvec(2) + wwr(3)*pvec(3))**2 +
     +          (wwi(1)*pvec(1) + wwi(2)*pvec(2) + wwi(3)*pvec(3))**2 
C ---     The difference gives sum of squares in the plane normal to pvec :
          wwpro3(idif,iev) = wwpro1(idif,iev) - wwpro2(idif,iev)  
          endif
        enddo     !   do idif=1,nodif
        write (io1,207) freq(iev),(wwpro1(idif,iev),idif=1,nodif)
        if (mode.eq.3) then
          write (io2,207) freq(iev),(wwpro2(idif,iev),idif=1,nodif)
          write (io3,207) freq(iev),(wwpro3(idif,iev),idif=1,nodif)
        endif
      enddo
      return
  201 format('#     Phonon eigenvectors for Q = (',3f10.6,' )',/
     .       '#          convoluted with qvec = (',3f10.6,' )')
  202 format('#  projected onto polariz. vector (',3f10.6,' )')
  203 format('#  projected onto plane normal to (',3f10.6,' )')
  204 format('#          and taken to square',/
     .       '#   Freq  ',2x,100(2x,i4,4x))
  205 format('#  (cm-1) ',2x,100(4x,a2,4x))
  206 format('#') 
  207 format (f10.3,100(f10.4))
      end
C
C...............................................................
C
      subroutine qres_smear(delta,npoints,io2,ivmin,ivmax,
     .                nat,jat,nodif,jtyp,label,freq,wwsum,zz)
C     Smear the peaks of phonon density of states 
C     (frequencies in  "freq", squared eigenvectors in "wwsum")
C     with a width parameter delta
C
      implicit none
      integer npoints,io2,iev,ivmin,ivmax,nodif,idif,ii,
     .        jtyp(nodif),nat,jat(nat,nat)
      double precision freq(ivmin:ivmax),wwsum(nodif,ivmin:ivmax),
     .       zz(nodif),xx,delta,xmin,xmax,xstep,broad
      character*2 label(nat)
      external broad

C     determine limits and mesh, to avoid many questions:
      xmin = freq(ivmin) - (freq(ivmax)-freq(ivmin))*0.15
      xmax = freq(ivmax) + (freq(ivmax)-freq(ivmin))*0.15
      xstep=(xmax-xmin)/(npoints-1)

      write (io2,202) (jtyp(idif),idif=1,nodif)
      write (io2,203) (label(jat(idif,1)),idif=1,nodif)
      write (io2,204) 
      do ii=1,npoints
        xx = xmin+(ii-1)*xstep
        do idif=1,nodif
          zz(idif)=0.d0
          do iev=ivmin,ivmax
              zz(idif)=zz(idif) +
     +                 broad(xx-freq(iev),delta)*wwsum(idif,iev)
          enddo
        enddo
        write (io2,205) xx,(zz(idif),idif=1,nodif)
      enddo
      return
  202 format('#   Freq  ',2x,100(2x,i4,4x))
  203 format('#  (cm-1) ',2x,100(4x,a2,4x))
  204 format('#') 
  205 format (f10.4,100(f10.4))
      end
C
C...............................................................
C
      subroutine symp_peaks(io1,nat,cosym,jat,label,qq,nodif,symfil,
     .                      jtyp,nna,ivmin,ivmax,freq,evr,evi,wwsum)
C
C     Multiplies eigenvectors by symmetry coefficients,
C     sums up over all atoms in type,
C     takes the result to square. 
C
      implicit none
      integer io1,iev,nodif,idif,jtyp(nat),nna(nat),
     .        nat,jat(nat,nat),nrat,ii,jj,ivmin,ivmax
      double precision freq(ivmin:ivmax),cosym(3,nat),
     .                 evr(3,nat,ivmin:ivmax),evi(3,nat,ivmin:ivmax),
     .                 wwsum(nodif,ivmin:ivmax),qq(3),wwr,wwi
      character*2 label(nat)
      character   symfil*60

      write (io1,201) qq,symfil(1:len_trim(symfil))
      write (io1,202) (jtyp(idif),idif=1,nodif)
      write (io1,203) (label(jat(idif,1)),idif=1,nodif)
      write (io1,204) 
C
      do iev=ivmin,ivmax
        do idif=1,nodif
          wwr=0.d0
          wwi=0.d0
          do jj=1,nna(idif)  !  loop over atoms within the same type
            nrat = jat(idif,jj)
            do ii=1,3
              wwr = wwr + evr(ii,nrat,iev)*cosym(ii,nrat)  
              wwi = wwi + evi(ii,nrat,iev)*cosym(ii,nrat)  
            enddo
          enddo
          wwsum(idif,iev) = wwr*wwr + wwi*wwi
        enddo
        write (io1,205) freq(iev),(wwsum(idif,iev),idif=1,nodif)
      enddo
      return
  201 format('#   Eigenvectors for Q =(',3f10.6,' )',/
     .       "#   projected with symm. coeffs from file '",(a),"',"/
     .       '#   and taken to square')
  202 format('#   Freq  ',2x,100(2x,i4,4x))
  203 format('#  (cm-1) ',2x,100(4x,a2,4x))
  204 format('#') 
  205 format (f10.3,100(f10.4))
      end
C
C...............................................................
C
      subroutine symp_smear(delta,npoints,io2,ivmin,ivmax,nat,jat,
     .                      qq,nodif,symfil,jtyp,label,freq,wwsum,zz)
C     Smear the peaks of phonon density of states 
C     (frequencies in  "freq", projected squared eigenvectors in "wwsum")
C     with a width parameter delta
C
      implicit none
      integer npoints,io2,iev,ivmin,ivmax,nodif,idif,ii,
     .        jtyp(nodif),nat,jat(nat,nat)
      double precision freq(ivmin:ivmax),wwsum(nodif,ivmin:ivmax),
     .       zz(nodif),xx,delta,xmin,xmax,xstep,broad,qq(3)
      character*2 label(nat)
      character   symfil*60

      external broad

C     determine limits and mesh, to avoid many questions:
      xmin = freq(ivmin) - (freq(ivmax)-freq(ivmin))*0.15
      xmax = freq(ivmax) + (freq(ivmax)-freq(ivmin))*0.15
      xstep=(xmax-xmin)/(npoints-1)

      write (io2,201) qq,symfil(1:len_trim(symfil)),delta
      write (io2,202) (jtyp(idif),idif=1,nodif)
      write (io2,203) (label(jat(idif,1)),idif=1,nodif)
      write (io2,204) 
      do ii=1,npoints
        xx = xmin+(ii-1)*xstep
        do idif=1,nodif
          zz(idif)=0.d0
          do iev=ivmin,ivmax
            zz(idif)=zz(idif) +
     +               broad(xx-freq(iev),delta)*wwsum(idif,iev)
          enddo
        enddo
        write (io2,205) xx,(zz(idif),idif=1,nodif)
      enddo
      return
  201 format('# ',/
     .       '#   Total phonon DOS for supercell Q = (',3f10.6,' )',/
     .       "#   projected with symm. coeffs from file '",(a),"',"/
     .       '#   taken to square and smeared with ',f8.4)
  202 format('#   Freq  ',2x,100(2x,i4,4x))
  203 format('#  (cm-1) ',2x,100(4x,a2,4x))
  204 format('#') 
  205 format (f10.4,100(f10.4))
      end
C
C...............................................................
C
      double precision function broad(xx,dd)
      implicit none
      double precision xx,dd
C     Explicitly defined broadening function:
C     e.g. Lorentz:
      broad=dd/3.1415926536/(xx*xx+dd*dd)
      return
      end
