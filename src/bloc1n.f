C...............................................................
C
      subroutine bloc1n(nz1,nz2,coor1,coor2,coor3,
     .                  udiag,uright,fcb)
C
C     makes (6*6) block of force constants
C     based on heuristic rules for nearest neighbors.
C
C     Input:
C       nz1          : Z-value for 1st atom
C       nz2          : Z-value for 2d  atom
C       coor1        : coordinates of 1st atom
C       coor2        : coordinates of 2d  atom
C       coor3        : coordinates of 3d (added) atom
C     Output:
C       fcb(1..6,1..6) : block of force constant matrix
C
      implicit none
      integer nz1,nz2,ii,jj,kk,ix,jx,io1
      double precision coor1(3),coor2(3),coor3(3),fcb(6,6),
     .                 vec1(3),vec2(3),vec3(3),vcnorm,dist,fcdiag(3),
     .                 udiag(6),uright(6,6),sqrt2
      data sqrt2/1.41421356237309504880/
      
      external param1n
C     
C     vec1: main axis, connecting atoms 1 and 2
       vec1 = coor2 - coor1
       vcnorm = sqrt( vec1(1)**2 + vec1(2)**2 + vec1(3)**2 )
       dist = vcnorm
       vec1 = vec1/(vcnorm * sqrt2)
C                   note that the "long" 6-eigenvectors
C                   must be normalized to 1, and not these "short" ones
C                   - see notex.tex
C
C     vec2: normal to the plane of three atoms
C             |        i                j                 k         |
C      vec2 = |  [coor2-coor1]_x  [coor2-coor1]_y  [coor2-coor1]_z  |
C             |  [coor3-coor1]_x  [coor3-coor1]_y  [coor3-coor1]_z  |
C
      vec2(1) = ( coor2(2)-coor1(2) )*( coor3(3)-coor1(3) ) -
     -          ( coor2(3)-coor1(3) )*( coor3(2)-coor1(2) )
      vec2(2) = ( coor2(3)-coor1(3) )*( coor3(1)-coor1(1) ) -
     -          ( coor2(1)-coor1(1) )*( coor3(3)-coor1(3) )
      vec2(3) = ( coor2(1)-coor1(1) )*( coor3(2)-coor1(2) ) -
     -          ( coor2(2)-coor1(2) )*( coor3(1)-coor1(1) )
       vcnorm = sqrt( vec2(1)**2 + vec2(2)**2 + vec2(3)**2 )
       vec2 = vec2/(vcnorm * sqrt2)
C
C     vec3: in the plane of three atoms; normal to vec1 and vec2
C
      vec3(1) = vec1(2)*vec2(3) - vec1(3)*vec2(2)
      vec3(2) = vec1(3)*vec2(1) - vec1(1)*vec2(3)
      vec3(3) = vec1(1)*vec2(2) - vec1(2)*vec2(1)
       vcnorm = sqrt( vec3(1)**2 + vec3(2)**2 + vec3(3)**2 ) 
       vec3 = vec3/(vcnorm * sqrt2)

      call param1n(nz1,nz2,dist,fcdiag)
C
C     recover 6*6 matrices explicitly (see my notes.tex):
      udiag(1) = -fcdiag(1)
      udiag(2) = -fcdiag(2)
      udiag(3) = -fcdiag(3)
      udiag(4) =  fcdiag(3)
      udiag(5) =  fcdiag(2)
      udiag(6) =  fcdiag(1)
C
      uright(1,1:3) =  vec1
      uright(1,4:6) =  vec1
      uright(2,1:3) =  vec2
      uright(2,4:6) =  vec2
      uright(3,1:3) =  vec3
      uright(3,4:6) =  vec3
      uright(4,1:3) = -vec3
      uright(4,4:6) =  vec3
      uright(5,1:3) = -vec2
      uright(5,4:6) =  vec2
      uright(6,1:3) = -vec1
      uright(6,4:6) =  vec1

      do ii= 1,6
      do jj= 1,6
        fcb(ii,jj) = 0.d0
        do kk = 1,6
          fcb(ii,jj) = fcb(ii,jj) +
     .                 uright(kk,ii)*udiag(kk)*uright(kk,jj)
        enddo
      enddo
      enddo

      return
      end
