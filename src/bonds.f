      program bonds
C
C       reads SIESTA XV file,
C       extracts bond lengths between two given types of atoms,
C       within a given interval of lengths.
C       An interactive input.
C       Several parameters are fixed in the code
C       and may need change:
C       mdisp = 2   : No. of trial translations along each lattice vector
C       parameter (dstep=0.0010) : discretization step 
C                  of the continuous distribution of bond lengths;
C       parameter (width=0.0025) : halfwidth parameter
C                  for artificial broadening of bond lengths.
C       Lorenztian broadening is assumed; change function broad(..,..)
C                  if want different.
C
C       Written by A. Postnikov, apostnik@uos.de
C       
      implicit none
      character ii1nam*60, io1nam*60, owrite*1
      integer ii1,io1,ii,jj,iat,jat,ityp,jtyp,
     .        nat,idisp,jdisp,kdisp,mdisp,ialloc,istep,nstep
      integer, allocatable :: ntyp(:), nz(:)
      double precision dx,dy,dz,dist,dmin,dmax,
     .                 xdisp,ydisp,zdisp,cc_bohr(3,3),scale
      double precision, allocatable :: coor_bohr(:,:)
      real, allocatable :: weight(:)
      parameter (scale=0.529177) ! conversion Bohr to Ang
      parameter (ii1=11,io1=13)
      logical filexist
C --- settings to write down a continuous distribution of bondlengths:
      double precision width,dstep,dlen,broad
      parameter (dstep=0.0002)
      parameter (width=0.0020)
      external broad
C
C --- open and read .XV:
      write (6,201,advance="no")
      read (5,*) ii1nam
      open (ii1,file=ii1nam,form='formatted',status='old',err=801)
      write (6,*) 'Found and opened: ',ii1nam
      do ii=1,3
        read  (ii1,101) (cc_bohr(jj,ii),jj=1,3)
      enddo
C     read (ii1,'(i13)') nat
      read (ii1,*) nat
      allocate (ntyp(nat),nz(nat),coor_bohr(3,nat),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      do iat=1,nat
        read (ii1,102,end=801,err=802) ntyp(iat),nz(iat),
     .                           (coor_bohr(ii,iat),ii=1,3)
      enddo
      close (ii1) 
      write (6,202,advance="no")
      read (5,*) ityp,jtyp
   11 continue
      write (6,203,advance="no")
      read (5,*) dmin,dmax
      if (dmin.lt.0..or.dmax.lt.0.) then
        write (6,*) '  dmax and dmin must be > 0'
        goto 11
      endif
      if (dmin.ge.dmax) then
        write (6,*) '  dmax must be larger than dmin'
        goto 11
      endif
C --- set up limits to write the continuous distribution:
      nstep = (dmax-dmin)/dstep + 1
      allocate (weight(nstep),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for weight(',nstep,').'
        stop
      endif
      weight(:)=0.
C
   13 write (6,204,advance="no")
      read (5,*) io1nam
      inquire (file=io1nam, exist=filexist)
      if (filexist) then
        write (6,*) ' File exist. Overwrite? (Y/N)'
        read (5,*) owrite
        if (owrite.eq.'Y'.or.owrite.eq.'y') then
          open (io1,file=io1nam,form='formatted',status='OLD')
        else
          goto 13
        endif
      else 
        open (io1,file=io1nam,form='formatted',status='NEW',err=803)
      endif
      write (io1, 301) ii1nam
      write (io1, 300) 
      do ii=1,3
        write (io1,302) (cc_bohr(jj,ii),jj=1,3)
      enddo
C     write (6,*) 'Allow  how many translations along each cell vector?'
C     read (5,*) mdisp
      mdisp = 2
      write (io1,303) ityp,jtyp,mdisp,dmin,dmax
      write (io1,304) 
C --- double loop over all atoms ---------------      
      do iat=1,nat
        if (ntyp(iat).eq.ityp) then
          do jat=1,nat
            if (ntyp(jat).eq.jtyp) then
C gets here if iat is of type 'ityp', jat of type 'jtyp'
C translate unit cell up to +/- mdisp along each vector:
              do idisp=-mdisp,mdisp
              do jdisp=-mdisp,mdisp
              do kdisp=-mdisp,mdisp
                xdisp = cc_bohr(1,1)*idisp + 
     +                  cc_bohr(1,2)*jdisp + 
     +                  cc_bohr(1,3)*kdisp
                ydisp = cc_bohr(2,1)*idisp + 
     +                  cc_bohr(2,2)*jdisp + 
     +                  cc_bohr(2,3)*kdisp
                zdisp = cc_bohr(3,1)*idisp + 
     +                  cc_bohr(3,2)*jdisp + 
     +                  cc_bohr(3,3)*kdisp
                dx = coor_bohr(1,iat)-coor_bohr(1,jat) + xdisp
                dy = coor_bohr(2,iat)-coor_bohr(2,jat) + ydisp
                dz = coor_bohr(3,iat)-coor_bohr(3,jat) + zdisp
                dist = sqrt(dx**2 + dy**2 + dz**2)*scale
                if (dist.lt.dmax.and.dist.gt.dmin) then
                  write(io1,305) dist,iat,
     .                           coor_bohr(1,iat),
     .                           coor_bohr(2,iat),
     .                           coor_bohr(3,iat),
     .                           idisp,jdisp,kdisp,jat,
     .                           coor_bohr(1,jat),
     .                           coor_bohr(2,jat),
     .                           coor_bohr(3,jat)
C --- add this bond to continuous distribution:
                  do istep=1,nstep
                    dlen = dmin + (istep-1)*dstep
                    weight(istep) = weight(istep) +
     +                              broad(dist-dlen,width)
                  enddo
                endif
              enddo
              enddo
              enddo
            endif
          enddo
        endif
      enddo
C --- write the obtained distribution of lengths as continuous function:
      write (io1,306)
      do istep=1,nstep
        dlen = dmin + (istep-1)*dstep
        write (io1,307) dlen, weight(istep)
      enddo
      close (io1)
      deallocate (ntyp,nz,coor_bohr)
      stop

  101 format (3x,3f18.9)
  102 format (i3,i6,3f18.9)

  201 format (' Full name of .XV file: ')
  202 format (' Two atom types for sampling bond length: ')
  203 format (' Max and min bond length (in Ang) to select: ')
  204 format (' Full name of the output file: ')

  301 format ('#   ',/
     .        '#   bond lengths analysis from the file ',/,
     .        '#   ',60a )
  300 format ('#   Lattice vectors (Bohr) : ')
  302 format ('#  ',3f16.9)
  303 format ('#   Bond lengths between atoms type ',i4,' and ',i4,/
     .        '#   translate up to +/- ',i3,/
     .        '#   Selected from ',f9.4,'  to ',f9.4,'  Ang',/
     .        '#  ')
  304 format ('# dist(Ang)  ',11('-'),' from ',11('-'),
     .        '  trans  ',12('-'),' to ',13('-')) 
  305 format ('#',f9.5,':',i3,3f9.4,' ',3i2,' ',i3,3f9.4)
  306 format ('#   Continuous distribution of lengths:',/
     .        '#   Ang            Weight')
  307 format (f10.5, f15.5)

  801 write (6,*) ' Error opening file as old formatted'
      stop
  802 write (6,*) ' End/Error reading XV for atom number ',iat
      stop
  803 write (6,*) ' Error opening file as new'
      stop
      end

      double precision function broad(xx,dd)
      implicit none
      double precision xx,dd
C       Explicitly defined broadening function:
C       e.g. Lorentz:
      broad=dd/3.1415926536/(xx*xx+dd*dd)
      return
      end

