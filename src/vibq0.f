      program vibq0
C
C       Standalone emulator of Vibra/vibrator program for q=(000),
C       independent on the fdf library and the main Siesta installation:
C       reads SIESTA .XV and .FC files and dumps the results
C       into .bands  and .vectors  
C
C       Written by A. Postnikov, andrei.postnikov@univ-lorraine.fr
C
      implicit none
      integer ii1,ii2,io1,io2
      parameter (ii1=11,ii2=12,io1=14,io2=15)
      integer ialloc,ierr,iat,jat,nat,ix,jx,ipm,iline,iind,jind,ndim,
     .        nghost
      integer, allocatable :: ityp(:),iz(:),nna(:)

      character inpfil*60,outfil*60,syslab*30
      double precision cc_ang(3,3),zeroo(3,3)
      double precision freq,conv,phid
C     conversion factor from dsqrt(K/M) in eV and Ang to cm**-1 is 519.6
      parameter (conv=519.6d0)   !  xmagic in Vibra/vibrator.f
      parameter (phid=-1.0d2)    !  diag elements for FC from ghost atoms
      double precision, allocatable :: mass(:),coor_ang(:,:),
     .     fc(:,:,:,:,:),phi(:,:,:,:),zero(:,:,:),dc(:,:),
     .     eval(:),evec1(:,:),fv1(:),fv2(:)
C --- diagonalization with HERMDP:    
C     double precision, allocatable :: dd(:,:),evec2(:,:),fv3(:,:)
C ---
      character*2, allocatable :: label(:)
      external test_xv,read_xv,rs,opnout,hermdp
C
C --- read lattice vectors and atom positions from the .XV file:
      write (6,"(' Specify SystemLabel of .XV and .FC files: ')",
     .       advance="no")
      read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.XV'
      open (ii1,file=inpfil,form='formatted',status='old',err=801)
      call test_xv(ii1,nat)
      ndim = nat*3
      allocate (ityp(nat),iz(nat))
      allocate (label(nat))
      allocate (mass(nat),coor_ang(1:3,1:nat),
     .          fc(1:3,1:nat,1:3,1:nat,1:2),phi(1:3,1:nat,1:3,1:nat),
     .          zero(1:3,1:3,1:nat),dc(1:ndim,1:ndim),
     .          eval(1:ndim),evec1(1:ndim,1:ndim),
     .          fv1(1:ndim),fv2(1:ndim),
C --- diagonalization with HERMDP:    
C    .     dd(1:ndim,1:ndim),evec2(1:ndim,1:ndim),fv3(1:2,1:ndim),
C ---
     .   STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
      call read_xv(ii1,nat,ityp,iz,cc_ang,mass,label,coor_ang)
      close (ii1)
C
C --- read force constants from the .FC file:
C     write (6,"(' Specify SystemLabel of .FC file: ')",advance="no")
C     read (5,*) syslab
      inpfil = syslab(1:len_trim(syslab))//'.FC'
      open (ii2,file=inpfil,form='formatted',status='old',err=801)
      iline = 0
      read (ii2,*)    
      do iat=1,nat   !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat   !   atom at which force is induced
          iline=iline + 1
          read (ii2,101,err=301,end=302) (fc(jx,jat,ix,iat,ipm),jx=1,3)
        enddo
      enddo
      enddo
      enddo
      close (ii2)
C
C --- Zero out force constants involving ghost atoms
C     and symmetrize the rest: 
      phi(:,:,:,:) = 0.d0
      nghost = 0
      do iat=1,nat
      do jat=1,nat
        if ((iz(iat).lt.0).and.(iat.eq.jat)) then
C ---     diagonal block of ghost atom; set fictive diagonal values:
	  nghost = nghost + 1
          phi(1,jat,1,iat) = phid
          phi(2,jat,2,iat) = phid
          phi(3,jat,3,iat) = phid
        elseif ((iz(iat).gt.0).and.(iz(jat).gt.0)) then
C ---     regular diagonal block
          do ix = 1,3
	  do jx = 1,3
            phi(jx,jat,ix,iat) = ( fc(jx,jat,ix,iat,1) +
     +                             fc(jx,jat,ix,iat,2) +
     +                             fc(ix,iat,jx,jat,1) +
     +                             fc(ix,iat,jx,jat,2) )/4
          enddo
          enddo
        endif
C ---   blocks between one ghost atom and any other one remain zero
      enddo
      enddo
      if (nghost.gt.0) then
        write(6,*) nghost,' ghost atoms found;'
        write(6,*) 'force constants involving them are zeroed out'
        write(6,*) 'and fictituous (big negative) diag. values assigned'
      endif
C --- Construct correction terms to enforce the acoustic sum rule (see notes):
      do iat=1,nat
        do ix =1,3
        do jx =1,3
          zero(jx,ix,iat) = 0.d0
          do jat=1,nat
            zero(jx,ix,iat) = zero(jx,ix,iat) + phi(jx,jat,ix,iat)
          enddo
          zero(jx,ix,iat) = zero(jx,ix,iat)/nat 
        enddo
        enddo
      enddo
      do ix =1,3
      do jx =1,3
        zeroo(jx,ix) = 0.d0
        do iat=1,nat
          zeroo(jx,ix) = zeroo(jx,ix) + zero(jx,ix,iat) 
        enddo
        zeroo(jx,ix) = zeroo(jx,ix)/nat 
      enddo
      enddo
C --- Introduce correction for acoustic sum rule, divide by masses,
C     construct the matrix in the global numbering:
      do iat=1,nat   !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
        iind=ix+(iat-1)*3
        do jat=1,nat   !   atom at which force is induced
        do jx =1,3     !   Cartesian displacements
          jind=jx+(jat-1)*3
          dc(iind,jind) = phi(jx,jat,ix,iat) +
     +                  ( zeroo(jx,ix)+zeroo(ix,jx) )/2 -
     -                  ( zero(jx,ix,iat)+zero(ix,jx,jat) )
          dc(iind,jind) = dc(iind,jind) / dsqrt( mass(iat)*mass(jat) )
        enddo
        enddo
      enddo
      enddo

C ... to diagonalize a symmetric matrix, call the RS sequence of EISPACK:
      call rs(ndim,ndim,dc,eval,1,evec1,fv1,fv2,ierr)
      write (6,*) ' Diagonalization with RS'
      if (ierr.ne.0) then
        write (6,*) ' RS diagonalization failed, ierr=',ierr
        stop
      endif
C ... or, call hermdp, like in Vibra/vibrator.f
C     Our matrix is real - still, hermdp needs to pass real and imag parts
C     do iind=1,3*nat
C     do jind=iind,3*nat
C       dd(iind,jind)=0.d0
C       dd(jind,iind)=dc(jind,iind)
C     enddo
C     enddo
C     call  hermdp(dd,evec1,evec2,eval,ndim,ndim,fv1,fv3,3)
C     write (6,*) ' Diagonalization with HERMDP'

C     Take sqrt, convert to cm-1, mark imaginary freqs. as negative:
      do iind=1,3*nat
        if (eval(iind).ge.0.d0) then
          freq =  conv * dsqrt(eval(iind))
        else
          freq = -conv * dsqrt(eval(iind)*(-1.d0))
        endif
        eval(iind) = freq
!       write(*,*)' eigenvalue #',iind,' omega=',eval(iind)
      enddo
C
C --  write eigenvalues as in Vibra/outbands.f :
      outfil = syslab(1:len_trim(syslab))//'.bands'
      call opnout(io1,outfil)
      write(io1,*) 0.d0
      write(io1,*) 0.d0, 0.d0
      write(io1,*) eval(1), eval(3*nat)
      write(io1,*) 3*nat, 1, 1
      write(io1,'(f10.6,10f12.4,/,(10x,10f12.4))')
     .      0.d0, (eval(iind),iind=1,3*nat)
      write(io1,*) 1
      write(io1,'(f12.6,3x,a3)') 0.d0, '''G'''
      close (io1)
C --  write eigenvectors as in Vibra/vibrator.f :
      outfil = syslab(1:len_trim(syslab))//'.vectors'
      call opnout(io2,outfil)
      write(io2,'(/,a,3f12.6)') 'k            = ',0.d0,0.d0,0.d0
      do iind=1,3*nat
        write(io2,'(a,i5)')     'Eigenvector  = ',iind
        write(io2,'(a,f12.6)')  'Frequency    = ',eval(iind)
        write(io2,'(a)')        'Eigenmode (real part)'
        write(io2,'(3e12.4)')   (evec1(jind,iind),jind=1,3*nat)
        write(io2,'(a)')        'Eigenmode (imaginary part)'
        write(io2,'(3e12.4)')   ( 0.d0, jind=1,3*nat)
      enddo
      close (io2)

      stop
  301 continue
      print *,' Error reading FC file, line ',iline
      stop
  302 continue
      print *,' Unexpected end of FC file, iat,ix,ipm,jat=',
     .         iat,ix,ipm,jat     
      stop
  101 format (3f15.7)

  801 write (6,*) ' Error opening file ',
     .            inpfil(1:len_trim(inpfil)),' as old formatted'
      stop
      end
