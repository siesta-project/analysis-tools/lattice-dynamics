C...............................................................
C
      subroutine sortnn(imod,nn1,nn2,nat,
     .                  cc_ang,coor_ang,dmax1,dmax2,
     .                  iat11,iat12,iat21,iat22,
     .                  coor11,coor12,coor21,coor22)
C
C     sorts out pairs of nearest neighbors (NN)
C     and next nearest neighbors (NNN),
C     taking into account lattice translations.
C
C     Input
C       imod  :  =0  counts neighbors (nn1,nn2) and returns
C                =1  counts neighbors and fill arrays
C       nat      : number of atoms
C       cc_ang   : translation vectors
C       coor_ang : coordinates of atoms
C       dmax1    : max. distance to NN
C       dmax2    : max. distance to NNN
C     Output
C       nn1      : number of pairs of NN
C       nn2      : number of pairs of NNN
C       iat11(1..nn1):  first atom in NN pair
C       iat12(1..nn1):  second atom in NN pair
C       iat21(1..nn2):  first atom in NN pair
C       iat22(1..nn2):  second atom in NN pair
C       coor11(1..3,1..nn1):  coordinates of first atom in NN pair
C       coor12(1..3,1..nn1):  coordinates of second atom in NN pair
C       coor21(1..3,1..nn2):  coordinates of first atom in NNN pair
C       coor22(1..3,1..nn2):  coordinates of second atom in NNN pair
C       Note: coordinates of the first atom are as in the original cell,
C             coordinaes of the second atom might include translation
C
      implicit none
      integer imod,nn1,nn2,nat
      integer iat11(nn1),iat12(nn1),iat21(nn2),iat22(nn2)
      double precision cc_ang(3,3),coor_ang(3,nat),
     .                 coor11(3,nn1),coor12(3,nn1),
     .                 coor21(3,nn2),coor22(3,nn2)
      integer mdisp,iat,jat,idisp,jdisp,kdisp
      double precision  xdisp,ydisp,zdisp,dx,dy,dz,dist,tiny,
     .                  dmax1,dmax2
      parameter (tiny = 10.d-8)

C   Count 1st and 2d neighbors, in order to allocate arrays:
      nn1 = 0
      nn2 = 0
C     write (6,*) 'Allow  how many translations along each cell vector?'
C     read (5,*) mdisp
      mdisp = 2
      do iat=1,nat
        do jat=iat,nat   
          do idisp=-mdisp,mdisp
          do jdisp=-mdisp,mdisp
          do kdisp=-mdisp,mdisp
            xdisp = cc_ang(1,1)*idisp + 
     +              cc_ang(1,2)*jdisp + 
     +              cc_ang(1,3)*kdisp
            ydisp = cc_ang(2,1)*idisp + 
     +              cc_ang(2,2)*jdisp + 
     +              cc_ang(2,3)*kdisp
            zdisp = cc_ang(3,1)*idisp + 
     +              cc_ang(3,2)*jdisp + 
     +              cc_ang(3,3)*kdisp
            dx = coor_ang(1,iat) - coor_ang(1,jat) - xdisp
            dy = coor_ang(2,iat) - coor_ang(2,jat) - ydisp
            dz = coor_ang(3,iat) - coor_ang(3,jat) - zdisp
            dist = sqrt(dx**2 + dy**2 + dz**2)
C           write (io1,301) iat,jat,dist
C 301 format (2i4,f12.6)
            if (dist.gt.tiny.and.dist.le.dmax1) then
C  add to 1st neigbors
              nn1 = nn1 + 1
              if (imod.eq.1) then
                iat11(nn1) = iat     
                iat12(nn1) = jat     
                coor11(1,nn1) = coor_ang(1,iat) 
                coor11(2,nn1) = coor_ang(2,iat) 
                coor11(3,nn1) = coor_ang(3,iat) 
                coor12(1,nn1) = coor_ang(1,jat) + xdisp
                coor12(2,nn1) = coor_ang(2,jat) + ydisp
                coor12(3,nn1) = coor_ang(3,jat) + zdisp
              endif
            elseif (dist.gt.dmax1.and.dist.le.dmax2) then
C  add to 2d neigbors
              nn2 = nn2 + 1
              if (imod.eq.1) then
                iat21(nn2) = iat     
                iat22(nn2) = jat     
                coor21(1,nn2) = coor_ang(1,iat) 
                coor21(2,nn2) = coor_ang(2,iat) 
                coor21(3,nn2) = coor_ang(3,iat) 
                coor22(1,nn2) = coor_ang(1,jat) + xdisp
                coor22(2,nn2) = coor_ang(2,jat) + ydisp
                coor22(3,nn2) = coor_ang(3,jat) + zdisp
              endif
            endif
          enddo
          enddo
          enddo
        enddo
      enddo
      return
      end
