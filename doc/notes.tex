\documentclass[12pt]{article}
\setlength{\textwidth}{16.0cm}
\setlength{\textheight}{22.0cm}
\usepackage{graphics}
\begin{document}
\centerline{\Large\bf{Explanatory notes on the Sies2LD set of tools}}
\centerline{Andrei Postnikov, {\tt apostnik@uos.de}}

\bigskip\noindent
{\large\bf{Dynamical equations, definitions, units}} \\*[2mm]
%
The force constant matrix, without the symmery analysis
(in terms of bare Cartesian displacements $X^{\alpha}_i$ of atoms
$\alpha$, $\beta$), however with the symmetry property
\[
D^{\alpha\beta}_{ij} = \frac{\partial^2 E_{\rm tot}}{%
\partial X^{\alpha}_i \,\partial X^{\beta}_j} = D^{\beta\alpha}_{ji}
\]
imposed, is
\begin{equation}
D^{\alpha\beta}_{ij} = \frac{1}{2} \left[
\frac{
F^{\alpha}_i(\{{\bf R}\}+d^{\beta}_j) -
F^{\alpha}_i(\{{\bf R}\}-d^{\beta}_j)
}{2d^{\beta}_j}
+
\frac{
F^{\beta}_j(\{{\bf R}\}+d^{\alpha}_i) -
F^{\beta}_j(\{{\bf R}\}-d^{\alpha}_i)
}{2d^{\alpha}_i}
\right]\,,
\label{eq:force}
\end{equation}
where $F^{\alpha}_i$ is the force on atom $\alpha$ in the direction $i$,
and $\{{\bf R}\}$+$d^{\beta}_j$ means that of all atoms, only the atom $\beta$
is displaced along $j$ from its equilibrium position by $d$
(that is MD.FCDispl in Siesta, e.g. the default $d$=0.04 Bohr).
The terms $[F^{\alpha}_i(\{{\bf R}\}+d^{\beta}_j)]/{d^{\beta}_j}$,
for positive and negative $d^{\beta}_j$ displacement along each Cartesian
direction, are calculated by Siesta and stored in the .FC file.
Their units are eV/{\AA}$^2$;
their reading can be organized as follows: 
\begin{verbatim}
      do iat=1,nat   !   atom which is displaced
      do ix =1,3     !   Cartesian displacements
      do ipm =1,2    !   displace + and -
        do jat=1,nat   !   atom at which force is induced
          read (ii2,'(3f15.7)') (fc(jx,jat,ix,iat,ipm),jx=1,3)
        enddo
      enddo
      enddo
      enddo
\end{verbatim}
and the symmetrization imposed as
\begin{verbatim}
      do iat=1,nat
      do ix =1,3
      do jat=1,nat
      do jx =1,3
        phi(jx,jat,ix,iat) = ( fc(jx,jat,ix,iat,1) +
     +                         fc(jx,jat,ix,iat,2) +
     +                         fc(ix,iat,jx,jat,1) +
     +                         fc(ix,iat,jx,jat,2) ) / 4
       enddo
       enddo
       enddo
       enddo
\end{verbatim}
With all elements of $D^{\alpha\beta}_{ij}$ in Eq.~(\ref{eq:force}) 
thus recovered,
the solution of dynamical equation
\begin{equation}
\sum_{\beta,k} \left[\,
\omega^2 \delta_{\alpha\beta}\delta_{ik} -
\frac{D^{\alpha\beta}_{ij}}{\sqrt{M_{\alpha}M_{\beta}}}\right]
%\left[\sqrt{M_{\beta}}\,
A^{\beta}_k
%\right]
= 0
\label{eq:dynam}
\end{equation}
yields zone-center phonon frequencies $\omega$ and eigenvectors
$A^{\alpha}_i$ of the supercell;  $A^{\alpha}_i$ = $\sqrt{M_{\alpha}}$
$\times$ (displacement amplitude). In order to transform to 
conventional units, use
\begin{verbatim}
      parameter (conv=519.6d0)     !   xmagic in Vibra/vibrator.f
      ...
      freq =  conv * dsqrt(eval(iind))
\end{verbatim}
where  {\tt eval} is eigenvalue of $\omega^2$ (Eq.~\ref{eq:dynam})
in eV/{\AA}$^2$, {\tt freq} -- phonon frequency in cm$^{-1}$. 
$\omega = 100$ cm$^{-1}$ corresponds to $\nu = 3.00$ THz or 
$\hbar\omega = 12.40$ meV.

\bigskip\noindent
{\large\bf{Imposing acoustic sum rule in Vibra/vibrator}} \\*[2mm]
%
The force constants matrix (\ref{eq:force}) is symmetric by consruction:
\[
D^{\alpha\beta}_{ij} = D^{\beta\alpha}_{ji}
\quad
\mbox{($\alpha$, $\beta$ = 1,$\cdots$,$N$: atoms; 
$i$, $j$: Cartesian coordinates).}
\]
Acoustic sum rule (absence of force under uniform displacement
of crystal) demands that
\[
\sum\limits_{\beta} D^{\alpha\beta}_{ij} = 0 
= 
\sum\limits_{\alpha} D^{\alpha\beta}_{ij}\,. 
\]
To enforce it, correct $D^{\alpha\beta}_{ij}$ as
\begin{eqnarray*}
\tilde{D}^{\alpha\beta}_{ij} &=&
D^{\alpha\beta}_{ij} - \left( Z^{\alpha}_{ij} + Z^{\beta}_{ji}\right)
+ \bar{Z}_{ij}\,, \\
\mbox{where}\quad Z^{\alpha}_{ij} &=&
\frac{1}{N}\sum\limits_{\beta} D^{\alpha\beta}_{ij}\,, \quad
Z^{\beta}_{ji} = \frac{1}{N}\sum\limits_{\alpha} D^{\beta\alpha}_{ji}\,, \\
\mbox{and}\quad \bar{Z}_{ij} &=&
\frac{1}{N}\sum\limits_{\alpha}Z^{\alpha}_{ij} =
\frac{1}{N}\sum\limits_{\beta}Z^{\beta}_{ji} = \bar{Z}_{ji}\,.
\end{eqnarray*}
Indeed,
\[
\bar{Z}_{ij} =
\frac{1}{N^2}\sum\limits_{\alpha\beta}D^{\alpha\beta}_{ij} =
\frac{1}{N^2}\sum\limits_{\alpha\beta}Z^{\beta\alpha}_{ji} = 
\frac{1}{N^2}\sum\limits_{\alpha\beta}Z^{\alpha\beta}_{ji}\,.  
\]
$\tilde{D}^{\alpha\beta}_{ij}$ obeys the acoustic sum rule, because
\begin{eqnarray*}
\sum\limits_{\beta}\tilde{D}^{\alpha\beta}_{ij} &=&
\underbrace{\sum\limits_{\beta}D^{\alpha\beta}_{ij}}_{NZ^{\alpha}_{ij}}
- N Z^{\alpha}_{ij} - N \bar{Z}_{ji} + N \bar{Z}_{ij} = 0\,, \\
\sum\limits_{\alpha}\tilde{D}^{\alpha\beta}_{ij} &=&
\underbrace{\sum\limits_{\alpha}D^{\alpha\beta}_{ij}}_{NZ^{\beta}_{ij}}
- N \bar{Z}_{ij} - N Z^{\beta}_{ji} + N \bar{Z}_{ij} = 0\,.
\end{eqnarray*}

\bigskip\noindent
{\large\bf{Diagonalization and visualization of force constants}} \\*[2mm]
%
One of the motivations in the development of the present package was
to make a sustainable procedure for a reasonable interpolation of
the force constants, proceeding just from the geometry 
of the relaxed supercell.
The hope that it is in principle possible is based on the following 
previous experience: 

1. For a number of (Zn,Be)Se supercells considered in our work
PRB {\bf 71}, 115206 (2005), there are quite clear trends that
diagonal elements of the force constants between 1st neighbors
and between the 2d neighbors show an uniform linear variation with
corresponding bond lengths, in apparent independence on concentration,
or local order (in fact the effect of the local order is indirectly
concealed in the bond lengths in question); 

2. Zeroing out of force constants beyond the 2d neighbors (as can be
done with the tool {\bf zrofc} of the present package) has a negligible
effect on the phonon spectrum. On the contrary, retaining just
the 1st neighbors (cation--anion force constants) turns out to be
too crude. 

From these observations is seems straightforward to provide a linear
fit for the diagonal elements of the 3$\times$3 force constants blocks
(between any chosen pair of atoms). However, the details of how does
the ``local coordinate system'' (in which the diagonalization is
achieved) looks like, and how one can recover the fitted force constants
in the global coordinate system, need special discussion. For better
analyzing these issues, the tools {\bf fcmat} and {\bf fcxsf} have been
designed.

The on-site terms $D^{\alpha\alpha}_{ij}$ are dominant in the force
constants matrix. They can be straightforwardly diagonalized,
yielding three values which are very close, and corresponding
eigenvectors. As the diagonal terms are almost equal, the potential well
is nearly spherical symmetric, so that the orientation of the local system
with respect to the global one is probably irrelevant. However, 
a more detailed analyzis on whether some anisotropy is important, and
what do the diagonal elements depend on (number of neighbors of one or
another kind, average distances to them?) may need further study.
The data for this are delivered by the script {\bf fcmat} in
readable form (straightforward on-site force constant marix,
the resulting eigenvalues and eigebvectors). These results can be
vizualized in a forn readable by XCrySDen, by using the script
{\bf fcxsf}. The eigenvectors scaled by corresponding eigenvalues are
represented by arrows, each arrow shown in +/$-$ direction
(as the sign of eigenvector is irrelevant). 

The case of two-center terms is more subtle. In fact the 3$\times$3
blocks of the force constants between two given atoms are not symmetric,
therefore the eigenvectors are, in general, complex. However, the symmetry
of the force constants $D^{\alpha\beta}_{ij} = D^{\beta\alpha}_{ji}$
means that the block matrix of the 6$\times$6 size, with two diagonal
3$\times$3 zero subblocks, yields real eigenvalues and eigenvectors.
The eigenvalues are pairwise symmetric, as three negative and three 
positive numbers. Of the corresponding eigenvectors, only three are
independent. It is easy to see from a 2$\times$2 diagonalization
of the matrix
\begin{equation}
{\bf A}\equiv\left(\begin{array}{cc} 0 & A \\ A & 0 \end{array}\right),
\quad\mbox{which by the force of}\quad
\mbox{det}\left|{\bf A} - \lambda\right| = 0
\end{equation}
\[
\mbox{has eigenvectors $\lambda = \pm A$, and corresponding eigenvectors}
\quad
\left(
\begin{array}{c} \frac{1}{\sqrt{2}} \\ ~\frac{1}{\sqrt{2}} \end{array}
\right); \quad
\left(
\begin{array}{c} \frac{1}{\sqrt{2}} \\ -\frac{1}{\sqrt{2}} \end{array}
\right). 
\]
If $A$ above is a 3$\times$3 block, the result is similar, yielding
(the eigenvectors $E_{1..3}$ in the increasing order) 
\begin{center}
\begin{tabular}{r@{~~:~~}c}
 $-E_3$ & {\small \begin{tabular}{|c|c|} 
 \hline \tt{vec1(1..3,3)} & ~~\tt{vec2(1..3,3)} \rule[0mm]{0mm}{4mm}\\ \hline 
 \end{tabular}} \\*[1mm]
 $-E_2$ & {\small \begin{tabular}{|c|c|} 
 \hline \tt{vec1(1..3,2)} & ~~\tt{vec2(1..3,2)} \rule[0mm]{0mm}{4mm}\\ \hline 
 \end{tabular}} \\*[1mm]
 $-E_1$ & {\small \begin{tabular}{|c|c|} 
 \hline \tt{vec1(1..3,1)} & ~~\tt{vec2(1..3,1)} \rule[0mm]{0mm}{4mm}\\ \hline 
 \end{tabular}} \\*[1mm]
 $ E_1$ & {\small \begin{tabular}{|c|c|} 
 \hline \tt{vec1(1..3,1)} & $-$\tt{vec2(1..3,1)} \rule[0mm]{0mm}{4mm}\\ \hline 
 \end{tabular}} \\*[1mm]
 $ E_2$ & {\small \begin{tabular}{|c|c|} 
 \hline \tt{vec1(1..3,2)} & $-$\tt{vec2(1..3,2)} \rule[0mm]{0mm}{4mm}\\ \hline 
 \end{tabular}} \\*[1mm]
 $ E_3$ & {\small \begin{tabular}{|c|c|} 
 \hline \tt{vec1(1..3,3)} & $-$\tt{vec2(1..3,3)} \rule[0mm]{0mm}{4mm}\\ \hline 
 \end{tabular}} \\*[1mm]
\end{tabular}
\end{center}
This is exactly what can be seen in the output file .FCmat
of the script {\bf fcmat}.

In order to understand the local coordinate syetem which diagonalizes
the force constants, we need, therefore, to analyze the orientation
of the vectors \tt{vec1} and \tt{vec2}, attributed to each
interatomic bond. In \tt{fcxsf.f}, these vectors are plotted
as arrows, scaled by corresponding eigenvalues, centered at the
points \tt{dmid1} and \tt{dmid2} correspondingly, which are slightly
displaced (for better visi\-bi\-li\-ty) from the midpoint of the bond:
\begin{verbatim}
             dmid1(:) = 0.6*cart1(:) + 0.4*cart2(:)
             dmid2(:) = 0.4*cart1(:) + 0.6*cart2(:)
               ...
              write (io1,'("  X",3f11.6,2x,3f11.6,2i4)')
     .                    (dmid1(jj),jj=1,3),
     .                    (vec1(ix,jx),ix=1,3),iat,jat
              write (io1,'("  X",3f11.6,2x,3f11.6,2i4)')
     .                    (dmid2(jj),jj=1,3),
     .                    (vec2(ix,jx),ix=1,3),iat,jat
\end{verbatim}

Given the \tt{vec1} and \tt{vec2} vectors, the force constants matrix
in the global setting can be recovered as

\vspace*{1.5cm}
\[
%\mbox{%
\begin{tabular}{|c|c|}
\hline
0 & $D^{(1)}_{ij}$ \rule[-3mm]{0mm}{9mm} \\
\hline
 $D^{(2)}_{ij}$ & 0 \rule[-3mm]{0mm}{9mm} \\
\hline
\end{tabular} 
= 
\rotatebox{-90}{%
\hspace*{-3.2cm}
{\footnotesize
\begin{tabular}{|c|c|}
\hline \tt{vec1(1..3,3)} & $-$\tt{vec2(1..3,3)} \\
\hline \tt{vec1(1..3,2)} & $-$\tt{vec2(1..3,2)} \\
\hline \tt{vec1(1..3,1)} & $-$\tt{vec2(1..3,1)} \\
\hline \tt{vec1(1..3,1)} & ~~\tt{vec2(1..3,1)} \\
\hline \tt{vec1(1..3,2)} & ~~\tt{vec2(1..3,2)} \\
\hline \tt{vec1(1..3,3)} & ~~\tt{vec2(1..3,3)} \\
\hline \end{tabular}
}} \,
{\small
\left(
\begin{array}{rrrrrr}
 \mbox{\hspace*{-2mm}}-E_3 & & & & & \\
 & \mbox{\hspace*{-8mm}}-E_2 & & & & \\
 & & \mbox{\hspace*{-8mm}}-E_1 & & & \\
 & & & \mbox{\hspace*{-6mm}}E_1  & & \\
 & & & & \mbox{\hspace*{-6mm}}E_2  & \\
 & & & & & \mbox{\hspace*{-6mm}}E_3  \\
\end{array}
\right)
} \,
{\footnotesize
\begin{tabular}{|c|c|}
\hline \tt{vec1(1..3,3)} & ~~\tt{vec2(1..3,3)} \\
\hline \tt{vec1(1..3,2)} & ~~\tt{vec2(1..3,2)} \\
\hline \tt{vec1(1..3,1)} & ~~\tt{vec2(1..3,1)} \\
\hline \tt{vec1(1..3,1)} & $-$\tt{vec2(1..3,1)} \\
\hline \tt{vec1(1..3,2)} & $-$\tt{vec2(1..3,2)} \\
\hline \tt{vec1(1..3,3)} & $-$\tt{vec2(1..3,3)} \\
\hline \end{tabular}
}
\]
That yields
\[
D^{(1)}_{ij} = -2\Bigl[ E_1\,{\tt vec1}(i,1)\,{\tt vec2}(j,1) +
                        E_2\,{\tt vec1}(i,2)\,{\tt vec2}(j,2) +
                        E_3\,{\tt vec1}(i,3)\,{\tt vec2}(j,3) \Bigr]\,;
\]
\[
D^{(2)}_{ij} = -2\Bigl[ E_1\,{\tt vec2}(i,1)\,{\tt vec1}(j,1) +
                        E_2\,{\tt vec2}(i,2)\,{\tt vec1}(j,2) +
                        E_3\,{\tt vec2}(i,3)\,{\tt vec1}(j,3) \Bigr]\,.
\]

 \end{document}
